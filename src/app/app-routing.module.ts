import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './layout/admin/admin.component';
import { AuthGuard } from './theme/authGuard/authGuard.service';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'parliament',
        loadChildren: "./theme/parliament/parliament.module#ParliamentModule"
      },
      {
        path: 'client',
        loadChildren: "./theme/client/client.module#ClientModule"
      },
      {
        path: 'statistics',
        loadChildren: "./theme/statistics/statistics.module#StatisticsModule"
      },
      {
        path: 'map',
        loadChildren: "./theme/map/map.module#MapModule"
      },
      {
        path: 'admin',
        loadChildren: "./theme/admin/admin.module#AdminModule"
      },
      {
        path: 'settings',
        loadChildren: "./theme/settings/settings.module#SettingsModule"
      },
      {
        path: 'provider',
        loadChildren: "./theme/provider/provider.module#ProviderModule"
      },
      {
        path: 'annauncement',
        loadChildren: "./theme/annauncement/annauncement.module#AnnauncementModule"
      },
      {
        path: 'sms',
        loadChildren: "./theme/sms/sms.module#SmsModule"
      }

    ]
  },
  {
    path: "login",
    loadChildren: "./theme/login/login.module#LoginModule"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
