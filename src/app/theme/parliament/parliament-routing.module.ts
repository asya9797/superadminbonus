import { NgModule } from "@angular/core";
import {  Routes,RouterModule } from "@angular/router";
import { ParliamentComponent } from "./parliament/parliament.component";


const parliamentRoutes : Routes = [
    {
        path: "",
        component: ParliamentComponent
    }
]

@NgModule({
imports:[RouterModule.forChild(parliamentRoutes)],
exports:[RouterModule]
})
export class ParliamentRoutingModule{}