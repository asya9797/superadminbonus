import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MessageService } from 'primeng/components/common/messageservice';
import { ParliamentService } from '../parliament.service';
@Component({
  selector: 'app-for-create-code',
  templateUrl: './for-create-code.component.html',
  styleUrls: ['./for-create-code.component.scss']
})
export class ForCreateCodeComponent implements OnInit {
  public addCodeForm: FormGroup;
  public loading: boolean = false;
  public forNegativeCode: string = undefined;
  public forCodeEdit: boolean = this.data.edit;
  public buttonValue: string;
  constructor(public dialogRef: MatDialogRef<ForCreateCodeComponent>,

    @Inject(MAT_DIALOG_DATA) public data: any,private _messageService: MessageService, private _parliamentService: ParliamentService ) { }

  ngOnInit() {
    this._buildForm();
    this.btnValue()
    
  }

  private _buildForm() {
  this.addCodeForm = new FormBuilder().group({
    code: ['', Validators.required]
  })
 

  }

  btnValue(){
    if(this.forCodeEdit){
      this.buttonValue = "Редактировать"
    }else{
      this.buttonValue = "Создать"
    }
  }
  

  postCode(){
     let sendingData = {
        ActiveCode: +this.addCodeForm.value.code,
        ParliamentId: this.data.item.Id
     }
     this.loading = true;
    this.addCodeForm.disable();
     this._parliamentService.forCreateCode(sendingData).subscribe((data:any) => {
       console.log(data);
       if(data.status){
          this.loading = false;
         this.addCodeForm.enable();
         if(!this.forCodeEdit){
          this._messageService.add({ severity: 'success', summary: '', detail: "Парламент уже имеет код" })
         }else{
          this._messageService.add({ severity: 'success', summary: '', detail: "Код был редактирован" })
         }
          this.dialogRef.close(true)
       }
     })
  }

}
