import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForCreateCodeComponent } from './for-create-code.component';

describe('ForCreateCodeComponent', () => {
  let component: ForCreateCodeComponent;
  let fixture: ComponentFixture<ForCreateCodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForCreateCodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForCreateCodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
