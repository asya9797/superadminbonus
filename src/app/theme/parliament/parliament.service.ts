import { Injectable } from "@angular/core";

import { ApiService } from "../../services/api.service";
@Injectable()
export class ParliamentService{


   
    constructor(private _apiService: ApiService){}
    
    getSortParliamentsBySale(first,second,page,count){
        return this._apiService.get(`getSortParliamentsBySale/${first}/${second}/${page}/${count}`);
    }

    sortBarByParliamentsIdBySale(first,second,id,page,count){
         return this._apiService.get(  `sortBarByParliamentsIdBySale/${first}/${second}/${id}/${page}/${count}`)
    }
    sortKassByBarIdBySale(first,second,page,count,id){
         return this._apiService.get(`get/kassa/${first}/${second}/${page}/${count}/${id}`)
    }
    resetDebtWithParliament(id,body){
        return this._apiService.put( `reset/debt/${id}`,body )
    }
    deleteParliament(id){
        return this._apiService.delete(`delete/parliament/${id}`)
    }
    filterParliamentsByDate(first,second,startDate,endDate,page,count){
        return this._apiService.get( `filterParliamentsByDate/${first}/${second}/${startDate}/${endDate}/${page}/${count}`)
    }
    filterBarByParliamentsIdByDate(first,second,startDate,endDate,id,page,count){
        return this._apiService.get( `filterBarByParliamentsIdByDate/${first}/${second}/${startDate}/${endDate}/${id}/${page}/${count}`)
    }
    filterKassByBarIdByDate(first,second,startDate,endDate,id,page,count){
        console.log(first)
        return this._apiService.get( `filterKassByBarIdByDate/${first}/${second}/${startDate}/${endDate}/${id}/${page}/${count}`)
    }
    getParliamentsCount(){
        return this._apiService.get('parliament/count');
    }
    getParliamentsBarCount(id){
        return this._apiService.get(`bar/count/${id}`);
    }
    filterParliamentsByDateCount(startDate,endDate){
        return this._apiService.get(`count/filterParliamentsByDate/${startDate}/${endDate}`)
    }
    filterBarByParliamentsIdByDateCount(startDate,endDate,id){
        return this._apiService.get(`count/filterBarByParliamentsIdByDate/${startDate}/${endDate}/${id}`)
    }
    getBarKassaCount(id){
        return this._apiService.get(`kassa/count/${id}`)
    }
    filterKassByBarIdByDateCount(startDate,endDate,id){
        return this._apiService.get(`count/filterKassByBarIdByDate/${startDate}/${endDate}/${id}`)
    }
    
    updateParliament(id,body){
        return this._apiService.put(`parliament/update/${id}`,body)
    }
     
    forCreateCode(body){
        return this._apiService.post(`parliament/code`,body)
    }
}