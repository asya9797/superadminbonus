import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KassesOfBarComponent } from './kasses-of-bar.component';

describe('KassesOfBarComponent', () => {
  let component: KassesOfBarComponent;
  let fixture: ComponentFixture<KassesOfBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KassesOfBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KassesOfBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
