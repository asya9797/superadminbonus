import { Component, OnInit, Input } from '@angular/core';
import { ParliamentService } from '../parliament.service';
import { AppService } from '../../../services/app.service';
import { DatePipe } from '@angular/common';
import { MessageService } from 'primeng/components/common/messageservice';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

Observable
@Component({
  selector: 'app-kasses-of-bar',
  templateUrl: './kasses-of-bar.component.html',
  styleUrls: ['./kasses-of-bar.component.scss']
})
export class KassesOfBarComponent implements OnInit {
  public arrowColor: string = 'white';
  public _openKassa: boolean;
  public totalCount: number = 0;
  public count: number = 4;
  public page: number = 0;
  public loading: boolean = false;
  public sortBy: string = 'Summa';
  public descent: string = '+';
  private _kassInfo: any;
  public kassesRangeDates: Array<Date>;
  public forFilterKass: boolean = false;
  public isCassesAvailable: boolean = false;
  public whenKassNone: boolean = false;
  public whenHaveKass: boolean = true;
  @Input() bar: Array<Object>;
  @Input() parliamentInfo: Array<Object>;
  @Input() barId: number;
  @Input('openKassa')
  set openKassa(value: boolean) {
    this._openKassa = value;
    if (this._openKassa) {
      this.getCassesData(this.sortBy, this.descent, this.barId)
    }
  }
  constructor(private _parliamentService: ParliamentService, private _appService: AppService,
    private _datePipe: DatePipe, private _messageService: MessageService) { }

  ngOnInit() {
  }
  //bar-i kassanery stanalu hamar e

  getBarKass(first, second, id) {
    this.sortBy = first;
    this.descent = second;
    return this._parliamentService.sortKassByBarIdBySale(first, second, this.page, this.count, id)
      .map((data: any) => {
        console.log(data);
        if (data.status) {
          for (let i of this.parliamentInfo) {
            for (let j of i["bar"]) {
              if (j.Id == id) {
                j["kassa"] = data.data;
              }
            }
          }
        }
        return data;
      })
      .catch((error) => {
        console.log("err ---------------- ", error);
        return Observable.of(false);
      })
  }


  //filtrac kassanery stanalu hamar e

  filterKassa(id) {
    this.forFilterKass = true;
    if (this.kassesRangeDates && this.kassesRangeDates[0] && this.kassesRangeDates[1]) {
      let startDate = this._datePipe.transform(this.kassesRangeDates[0], 'yyyy-MM-dd');
      let endDate = this._datePipe.transform(this.kassesRangeDates[1], 'yyyy-MM-dd');
      this._parliamentService.filterKassByBarIdByDate(this.sortBy, this.descent, startDate, endDate, id, this.page, this.count).subscribe((data: any) => {
        this._kassInfo = data.data;
        console.log("filtrac kassery----", this._kassInfo)
      })
    }
  }


  //bari kassaneri county stanalu hamar

  private _getKassaCount(id) {
    return this._parliamentService.getBarKassaCount(id).map((data: any) => {
      this.totalCount = data.data;
      this.isCassesAvailable = true;
      console.log("kasses counttttt", this.totalCount)
      return data;
    })
  }

  public getCassesData(first, second, id) {
    this.loading = true;
    const combined = Observable.forkJoin(
      this._getKassaCount(id),
      this.getBarKass(first, second, id)
    )
    combined.subscribe((data) => {
      console.log(data);
      this.loading = false;
    })
  }


  //filtrac kasseri yndhanur qanaky stanalu hamar

  private _getFilterKassaCount(id) {
    if (this.kassesRangeDates && this.kassesRangeDates[0] && this.kassesRangeDates[1]) {
      let startDate = this._datePipe.transform(this.kassesRangeDates[0], 'yyyy-MM-dd');
      let endDate = this._datePipe.transform(this.kassesRangeDates[1], 'yyyy-MM-dd');
       return this._parliamentService.filterKassByBarIdByDateCount(startDate, endDate, id).map((data: any) => {
        this.totalCount = data.data
      })
    }
  }


  //bareri kassa-nery ejerov cuyc talu hamar e

  paginateKass(event, id) {
    this.count = event.pageSize;
    this.page = event.pageIndex;
    if (!this.forFilterKass) {
      return this._parliamentService.sortKassByBarIdBySale(this.sortBy, this.descent, this.page, this.count, id).map((data: any) => {
        if (data.status) {
          for (let i of this.parliamentInfo) {
            for (let j of i["bar"])
              if (j.Id == id) {
                j["kassa"] = data.data
              }
          }
        }
      })
    }
    if (this.forFilterKass) {
      if (this.kassesRangeDates && this.kassesRangeDates[0] && this.kassesRangeDates[1]) {
        let startDate = this._datePipe.transform(this.kassesRangeDates[0], 'yyyy-MM-dd');
        let endDate = this._datePipe.transform(this.kassesRangeDates[1], 'yyyy-MM-dd');
          return this._parliamentService.filterKassByBarIdByDate(this.sortBy, this.descent, startDate, endDate, id, this.page, this.count).map((data: any) => {
          this._kassInfo = data.data;
        })
      }
    }
  }

  public getPaginateCassesData(event,id){
    this.loading = true;
    const combinate =  Observable.forkJoin(
      this._getFilterKassaCount(id),
      this.paginateKass(event,id)
    )
    combinate.subscribe((data) => {
      console.log(data);
      this.loading = false;
    })
  }


  //kasseri filtri inputi arjeqy maqrelu hamar e

  deleteKassDate() {
    this.kassesRangeDates = undefined;
    this.forFilterKass = false;
    this.getCassesData(this.sortBy, this.descent, this.barId);
  }

}
