import { Component, OnInit } from '@angular/core';
import { ParliamentService } from '../parliament.service';
import { MatDialog } from '@angular/material';
import { AppService } from '../../../services/app.service';
import { DatePipe } from '@angular/common';
import { MessageService } from 'primeng/components/common/messageservice';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { ForCreateCodeComponent } from '../for-create-code/for-create-code.component';

@Component({
  selector: 'app-parliament',
  templateUrl: './parliament.component.html',
  styleUrls: ['./parliament.component.scss'
  ]
})
export class ParliamentComponent implements OnInit {
  public totalCount: number = 0;
  public count: number = 10;
  public page: number = 0;
  public isParliamentsAvailable: boolean = false;
  public loading: boolean = false;
  public sortBy: string = 'Summa';
  public descent: string = '+';
  public panelOpenState: boolean = false;
  public _parliamentsInfo: any;
  public parliamentsRangeDates: Array<Date>;
  public selected: any;
  public arrowColor: string = 'white';
  public on: boolean;
  public off: boolean;
  public activateOrDeactivate: boolean;
  public isActiveOpacity: number = 1;
  public forFilterParliament: boolean = false;

  constructor(private _parliamentService: ParliamentService, private _appService: AppService,
    private _datePipe: DatePipe, private _messageService: MessageService, private dialog: MatDialog) { }

  ngOnInit() {
    this.getParliamentsData(this.sortBy, this.descent);
    this._getFilterParliamentsCount();
  }


  //parliamentnerin stanalu hamar e 

  getParliaments(first, second) {
    this.sortBy = first;
    this.descent = second;
    return this._parliamentService.getSortParliamentsBySale(first, second, this.page, this.count).map((data: any) => {
      console.log(data);
      this._parliamentsInfo = data.data;
      for (let i of this._parliamentsInfo) {
        i["bar"] = [];
        i["open"] = false;
      }
      console.log(this._parliamentsInfo)
      return data;
    }).catch((error) => {
      console.log("err-----", error)
      return Observable.of(false)
    })
  }

  // parlamentneri yndhanur qanaky stanalu hamar e

  private _getParliamentsCount() {
    return this._parliamentService.getParliamentsCount().map((data: any) => {
      console.log(data);
      this.totalCount = data.data;
      this.isParliamentsAvailable = true;
      return data
    })
  }

  public getParliamentsData(first, second) {
    this.loading = true;
    const combined = Observable.forkJoin(
      this._getParliamentsCount(),
      this.getParliaments(first, second)
    )
    combined.subscribe((data) => {
      console.log(data);
      this.loading = false;
    })
  }



  //parlamentnery filtrac,yst oreri stanalu hamar e

  filterParliaments() {
    this.forFilterParliament = true;
    if (this.parliamentsRangeDates && this.parliamentsRangeDates[0] && this.parliamentsRangeDates[1]) {
      let startDate = this._datePipe.transform(this.parliamentsRangeDates[0], 'yyyy-MM-dd');
      let endDate = this._datePipe.transform(this.parliamentsRangeDates[1], 'yyyy-MM-dd');
      this._parliamentService.filterParliamentsByDate(this.sortBy, this.descent, startDate, endDate, this.page, this.count).subscribe((data: any) => {
        this._parliamentsInfo = data.data;
        console.log("filtracc parlamentnerr", this._parliamentsInfo)
      })
    }
  }

  // filtrac parliamentneri yndhanur qanaky stanalu hamar e

  private _getFilterParliamentsCount() {
    if (this.parliamentsRangeDates && this.parliamentsRangeDates[0] && this.parliamentsRangeDates[1]) {
      let startDate = this._datePipe.transform(this.parliamentsRangeDates[0], 'yyyy-MM-dd');
      let endDate = this._datePipe.transform(this.parliamentsRangeDates[1], 'yyyy-MM-dd');
      this._parliamentService.filterParliamentsByDateCount(startDate, endDate).subscribe((data: any) => {
        this.totalCount = data.data;
        console.log("count of filter parliament", this.totalCount)
      })
    }
  }


  // parliamenti u filtrac parliamenti tvyalnery ejerov cuyc talu hamar e 

  public paginate(event) {
    this.count = event.pageSize;
    this.page = event.pageIndex;
    if (!this.forFilterParliament) {
      this._parliamentService.getSortParliamentsBySale(this.sortBy, this.descent, this.page, this.count).subscribe((data: any) => {
        this._parliamentsInfo = data.data;
        for (let i of this._parliamentsInfo) {
          i["bar"] = []
        }
      })
    }
    if (this.forFilterParliament) {
      if (this.parliamentsRangeDates && this.parliamentsRangeDates[0] && this.parliamentsRangeDates[1]) {
        let startDate = this._datePipe.transform(this.parliamentsRangeDates[0], 'yyyy-MM-dd');
        let endDate = this._datePipe.transform(this.parliamentsRangeDates[1], 'yyyy-MM-dd');
        this._parliamentService.filterParliamentsByDate(this.sortBy, this.descent, startDate, endDate, this.page, this.count).subscribe((data: any) => {
          this._parliamentsInfo = data.data;
          this._getFilterParliamentsCount()

        })

      }
    }
  }


  // parliamenty jnjelu hamar e

  deleteParliamentById(id) {
    this._parliamentService.deleteParliament(id).subscribe((data: any) => {
      this.getParliamentsData(this.sortBy, this.descent);
      this._messageService.add({ severity: 'success', summary: '', detail: "Этот парламент был удален" })
    })
  }

  onClickDelete(id): void {
    this._appService.confirmDialog().subscribe((data) => {
      if (data && data.approve) {
        this.deleteParliamentById(id);
      }
    })
  }





  // parliamenti debt-y reset enelu hamar e

  resetDebt(id) {
    console.log("jhsgdshgfdhagd", id)
    this._parliamentService.resetDebtWithParliament(id, {}).subscribe((data: any) => {
      console.log(data);
      this.getParliamentsData(this.sortBy, this.descent);
      this._messageService.add({ severity: 'success', summary: '', detail: "Сброшена" })
    }, (err) => {
      console.log(err)

      // if (err.error.data.message == "parliament not changed, because your sending data is already exists in db") {
      //   this._messageService.add({ severity: 'error', summary: '', detail: "parliament not changed, because your sending data is already exists in db" })
      // }
    })
  }
  onClickDebtEdit(id) {
    this._appService.confirmDialog().subscribe((data) => {
      if (data && data.approve) {
        this.resetDebt(id)

      }
    })
  }


  // parliamenti active/deactive-neri hamar e

  clickToggle(item) {
    console.log("radikkkk")
   
      this._parliamentService.updateParliament(item.Id, { IsActive: (item.IsActive) ? 0 : 1 })
        .subscribe((data) => {
          console.log(data)
          this.loading = false;
          this.getParliamentsData(this.sortBy, this.descent)
        })
    



  }


  //parlamenti filtri inputi arjeqy maqrelu hamar e

  deleteParliamentDate() {
    this.parliamentsRangeDates = undefined;
    this.forFilterParliament = false;
    this.getParliamentsData(this.sortBy, this.descent);
  }

  //partqi kam avelcuki guinery cuyc talu hamar e 

  getStyle(item) {
    if (item.Dept < 0) {

      return {
        'color': 'red'
      }
    } else {
      return { 'color': 'green' }
    }
  }
  openDialog(data,forEdit): void {

    const dialogRef = this.dialog.open(ForCreateCodeComponent, {
      width: '450px',
      data: {
        item: data,
        edit: forEdit      
      },
      panelClass: "forPadding",

    });
    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.getParliamentsData(this.sortBy,this.descent)
      }

    });
  }



}






