import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BarsOfParliamentComponent } from './bars-of-parliament.component';

describe('BarsOfParliamentComponent', () => {
  let component: BarsOfParliamentComponent;
  let fixture: ComponentFixture<BarsOfParliamentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BarsOfParliamentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BarsOfParliamentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
