import { Component, OnInit,Input } from '@angular/core';
import { ParliamentService } from '../parliament.service';
import { AppService } from '../../../services/app.service';
import { DatePipe } from '@angular/common';
import { MessageService } from 'primeng/components/common/messageservice';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
@Component({
  selector: 'app-bars-of-parliament',
  templateUrl: './bars-of-parliament.component.html',
  styleUrls: ['./bars-of-parliament.component.scss']
})
export class BarsOfParliamentComponent implements OnInit {
public arrowColor: string = 'white';
public _open:boolean = false;
public totalCount: number = 0;
public count: number = 5;
public page: number = 0;
public loading: boolean = false;
public sortBy: string = 'Summa';
public descent: string = '+';
private _barInfo: any;
public barsRangeDates: Array<Date>;
public forFilterBar:boolean = false;
public isBarsAvailable: boolean = false;
@Input() parliament:Array<Object>;
@Input() parliamentInfo: Array<Object>;
@Input() parliamentId: number;
@Input('open')
         set open(value: boolean){
           this._open = value;
           if(this._open){
             this.getBarsData(this.sortBy,this.descent,this.parliamentId)
           }
           
         }

constructor(private _parliamentService: ParliamentService, private _appService: AppService,
  private _datePipe: DatePipe, private _messageService: MessageService){}
ngOnInit(){
 
}

//parliamenti bar-ery stanalu hamar e  

getParliamentsBar(first, second, id) {
  this.sortBy = first;
  this.descent = second;
  return this._parliamentService.sortBarByParliamentsIdBySale(first, second, id, this.page, this.count).map((data: any) => {
    if (data.status) {
      for (let i of this.parliamentInfo) {
        if (i['Id'] == id) {
          i["bar"] = data.data;
          i["bar"]["openKassa"] = false;
        }
      }
    }
    
    return data;
  }).catch( (error) => {
    console.log("err ---------------- ", error)
    return Observable.of(false)
  })
}


//parliamenti bar-eri qanaky stanalu hamar e 

private _getBarCount(id) {
  return this._parliamentService.getParliamentsBarCount(id).map((data: any) => {
    this.totalCount = data.data;
    this.isBarsAvailable = true;
    console.log("bar count", this.totalCount)
    return data;
  })
}


public getBarsData(first, second, id){
  this.loading = true;
  const combined = Observable.forkJoin(
    this._getBarCount(id),
    this.getParliamentsBar(first, second, id)
  )
  combined.subscribe((data) => {
    console.log(data);
    this.loading = false;
  })
  
}



//filtrac bareri yndhanur qanaky stanalu hamar

private _getFilterBarsCount(id) {
  if (this.barsRangeDates && this.barsRangeDates[0] && this.barsRangeDates[1]) {
    let startDate = this._datePipe.transform(this.barsRangeDates[0], 'yyyy-MM-dd');
    let endDate = this._datePipe.transform(this.barsRangeDates[1], 'yyyy-MM-dd');
    return this._parliamentService.filterBarByParliamentsIdByDateCount(startDate, endDate,id).map((data: any) => {
      this.totalCount = data.data;
      console.log("bereri countyyy", this.totalCount)
    })
  }
}


//filtrac barery stanalu hamar e

filterBars(id) {
  this.forFilterBar = true;
  if (this.barsRangeDates && this.barsRangeDates[0] && this.barsRangeDates[1]) {
    let startDate = this._datePipe.transform(this.barsRangeDates[0], 'yyyy-MM-dd');
    let endDate = this._datePipe.transform(this.barsRangeDates[1], 'yyyy-MM-dd');
    this._parliamentService.filterBarByParliamentsIdByDate(this.sortBy, this.descent, startDate, endDate, id, this.page, this.count).subscribe((data: any) => {
      this._barInfo = data.data;
      console.log("filtrac barery----", this._barInfo)
    })
  }
}

//parliamenti bar-ery ejerov cuyc talu hamar e

public paginateBar(event,id) {
  console.log("event is", event);
  this.count = event.pageSize;
  this.page = event.pageIndex;
  if (!this.forFilterBar) {
    return this._parliamentService.sortBarByParliamentsIdBySale(this.sortBy, this.descent, id, this.page, this.count).map((data: any) => {
      if (data.status) {
        for (let i of this.parliamentInfo) {
          if (i['Id']== id) {
            i["bar"] = data.data
          }
        }
      }
    })
  }
  if (this.forFilterBar) {
    if (this.barsRangeDates && this.barsRangeDates[0] && this.barsRangeDates[1]) {
      let startDate = this._datePipe.transform(this.barsRangeDates[0], 'yyyy-MM-dd');
      let endDate = this._datePipe.transform(this.barsRangeDates[1], 'yyyy-MM-dd');
       return this._parliamentService.filterBarByParliamentsIdByDate(this.sortBy, this.descent, startDate, endDate, id, this.page, this.count).map((data: any) => {
        this._barInfo = data.data;
      })
    }
  }
}


public getPaginateBarsData(event, id){
  this.loading = true;
  const combined = Observable.forkJoin(
    this._getFilterBarsCount(id),
    this.paginateBar(event, id)
  )
  combined.subscribe((data) => {
    console.log(data);
    this.loading = false;
  })
  
}

//barerii filtri inputi arjeqy maqrelu hamar e

deleteBarDate() {
  this.barsRangeDates = undefined;
  this.forFilterBar = false;
  this.getBarsData(this.sortBy, this.descent,this.parliamentId);
}

}
