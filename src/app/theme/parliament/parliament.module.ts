import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ParliamentService } from "./parliament.service";
import { ParliamentRoutingModule } from "./parliament-routing.module";
import { ParliamentComponent } from "./parliament/parliament.component";
import { MaterialModule } from "../../shared/material/material.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CookieService } from "ngx-cookie-service";
import { CollapsibleModule } from 'angular2-collapsible';
import { BarsOfParliamentComponent } from './bars-of-parliament/bars-of-parliament.component';
import { KassesOfBarComponent } from './kasses-of-bar/kasses-of-bar.component';
import { SharedModule } from "../../shared/shared.module";
import { ForCreateCodeComponent } from './for-create-code/for-create-code.component';



@NgModule({
    declarations:[ParliamentComponent, BarsOfParliamentComponent, KassesOfBarComponent, ForCreateCodeComponent],
    imports:[CollapsibleModule,CommonModule,ParliamentRoutingModule,MaterialModule,ReactiveFormsModule,FormsModule,SharedModule],
    exports:[],
    entryComponents: [ForCreateCodeComponent],
    providers:[ParliamentService,CookieService]
})
export class ParliamentModule{}