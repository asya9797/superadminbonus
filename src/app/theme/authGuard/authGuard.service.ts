import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import "rxjs/add/observable/of";
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
@Injectable()
export class AuthGuard implements CanActivate {
    //private _baseURL = 'http://46.101.179.50:3000';
    private _baseURL = 'http://46.101.179.50:3000/token'
    constructor(private _httpClient: HttpClient, private cookieService: CookieService, private router: Router) { }

    canActivate(): Observable<boolean> | Promise<boolean> | boolean {
        return this.checkAccessToken()
    }



    checkAccessToken() {
        return this._httpClient.post(this._baseURL + '/checkAccess', {
            token: this.cookieService.get('adminToken'),
            refreshToken: this.cookieService.get('adminRefreshToken'),
            table: 'admin'
        }, { observe: 'response' })
            .map((data: any) => {
                if (data.status == 200) {
                    if (data.body.status == false) {
                        this.cookieService.put('adminToken', data.body.data.token);
                    }
                    return true;
                }
                else {
                    return false;
                }
            })
            .catch((error) => {
                this.router.navigate(['/login'])
                return Observable.throw(false);
            })
    }
}