import { Injectable } from "@angular/core";
import { ApiService } from "../../services/api.service";
import { HttpClient } from "@angular/common/http";

@Injectable()
export class LoginService{
    private baseUrl:string = "http://46.101.179.50:3000/admin/";
    constructor(private _httpClient: HttpClient){}


    login(userinfo){
        console.log("userinfo is   " ,userinfo)
        return this._httpClient.post( this.baseUrl + "login", userinfo);
    }
}