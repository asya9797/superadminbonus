import { NgModule } from "@angular/core";
import { LoginService } from "./login.service";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LoginRoutingModule } from "./login-routing.module";
import { LoginComponent } from "./login/login.component";
import { HttpClientModule } from "@angular/common/http";
import { CookieService } from 'ngx-cookie-service';
import { MaterialModule } from "../../shared/material/material.module";
import { SharedModule } from "../../shared/shared.module";


@NgModule({
    declarations:[LoginComponent],
    imports:[CommonModule,FormsModule,ReactiveFormsModule,LoginRoutingModule,HttpClientModule,MaterialModule,SharedModule],
    exports:[],
    providers:[LoginService,CookieService ]
})
export class LoginModule{

}