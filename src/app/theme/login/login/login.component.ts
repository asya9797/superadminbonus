import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../login.service';
import { CookieService } from 'ngx-cookie-service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public message: string = undefined;
  public loading: boolean = false;
  public loginForm: FormGroup;
  constructor(private _router: Router, private _loginService: LoginService, private _cookieService: CookieService) { }

  ngOnInit() {
    this._buildForm()
  }

  public adminSignIn(): void {
    this.loading = true;
    this.loginForm.disable();
    this._loginService.login({
      Username: this.loginForm.value.username,
      Password: this.loginForm.value.password,
    }).subscribe((response: any) => {
      console.log(response)
      if (response.status && response.data.token) {
        console.log(response);
        this._cookieService.set("adminToken", response.data.token)
        this._cookieService.set("adminRefreshToken", response.data.refreshToken)
        this._router.navigate(["parliament"]);
        this.loading = false;
        this.loginForm.enable();
      }

    }, (err) => {
      this.message = "Пожалуйста, введите свои данные правильно";
      this.loading = false;
      this.loginForm.enable();
    })
  }

  private _buildForm() {
    this.loginForm = new FormBuilder().group({
      username: ['', Validators.required],
      password: ['', Validators.required]

    })
  }

}
