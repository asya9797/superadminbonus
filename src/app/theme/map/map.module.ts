import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MapRoutingModule } from "./map-routing.module";
import { MapService } from "./map.service";
import { MapComponent } from "./map/map.component";
import { MaterialModule } from "../../shared/material/material.module";

@NgModule({
    declarations: [MapComponent],
    imports:[CommonModule,MapRoutingModule,MaterialModule],
    exports:[],
    providers:[MapService]
})
export class MapModule{}