import { Injectable } from "@angular/core";
import { ApiService } from '../../services/api.service';

@Injectable()
export class MapService{
    
    constructor(private _apiService:ApiService){}

    public getAllBars(){
        return this._apiService.get('getAll/bars')
    }
}