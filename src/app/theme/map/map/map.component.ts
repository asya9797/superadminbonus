import { Component, OnInit } from '@angular/core';
import { MapService } from '../map.service';

declare var google: any;

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {
  public loading: boolean = false;
  private _allBars: Array<object> = [];
  private _map: any;
  constructor(private _mapService: MapService) { }

  ngOnInit() {
    this.initMap();
    this._getAllBars();
  }


  initMap() {
    this._map = new google.maps.Map(document.getElementById('map'), {
      center: { lat: 40.926038, lng: 43.851647 },
      zoom: 3
    });
  }


  private _getAllBars() {
    this.loading = true;
    this._mapService.getAllBars().subscribe((data: any) => {
      console.log(data);
      this._allBars = data.data;
      this.setMapMarkers(this._allBars, this._map);
      this.loading = false
    })
  }

  private _addMarker(location, map,item) {
    let url = '/assets/def.png';
    if(item.Image){
      //url = 'http://46.101.179.50:3000/static/'+ item.Image
      url = 'http://46.101.179.50:3000/static/' + item.Image
    }
    var contentString = '<div id="content">' +
      '<div id="siteNotice">' +
      '</div>' +
      '<h4 id="firstHeading" class="firstHeading">'+item.Name+'</h4>' +
      '<div id="bodyContent">' +
      '<p><b>Адрес: </b> ' + item.Address_text + '</p>' +
      '<div style="width:100px;height:100px;background-size:cover;background-position:center;background-repeat:no repeat;background-image:url('+url+')"></div>'+
      '</div>' +
      '</div>';

    var infowindow = new google.maps.InfoWindow({
      content: contentString
    });
    var marker = new google.maps.Marker({
      position: location,
      map: map
    });
    marker.addListener('mouseover', function() {
      infowindow.open(map, marker);
  });
  
  // assuming you also want to hide the infowindow when user mouses-out
  marker.addListener('mouseout', function() {
      infowindow.close();
  });
  }

  private setMapMarkers(bars, map) {
    console.log(bars)
    bars.forEach((item) => {
      if (item.Lat && item.Lng) {

        let coordinates = {
          lat: item.Lat,
          lng: item.Lng
        };

        if (coordinates.lat && coordinates.lng) {
          this._addMarker(coordinates, map,item);
        }

      };
    })

  }


}