import { Injectable } from "@angular/core";
import { ApiService } from "../../services/api.service";

@Injectable()
export class StatisticsService{

    constructor(private _apiService: ApiService){}

    public getSortParliamentsBySale(first,second){
        return this._apiService.get(`getSortParliamentsBySale/${first}/${second}`);
    }
    /**
     * 
     * @param first 
     * @param second 
     * @param startDate 
     * @param endDate 
     */
    public filterParliamentByDate(first,second,startDate="2018-07-04",endDate="2018-07-30"){
        return this._apiService.get(`filterParliamentsByDate/${first}/${second}/${startDate}/${endDate}`);
    }
    public filterParliamentsByDateDays(id){
        return this._apiService.get(`filterParliamentsByDateDays/${id}`);
    }
    public  filterClientByDateDays(id){
        return this._apiService.get(`filterClientByDateDays/${id}`);
    }
    getClientCount(){
        return this._apiService.get('client/count')
    }
    getParliaments(){
        return this._apiService.get("transaction/statistic")
    }
}