import { NgModule } from "@angular/core";
import { StatisticsService } from "./statistics.service";
import { CommonModule, DatePipe } from "@angular/common";
import { StatisticsRoutingModule } from "./statistics-routing.module";
import { StatisticsComponent } from "./statistics/statistics.component";
import { SharedModule } from "../../shared/shared.module";
import {ChartModule} from 'angular2-chartjs';
import { MaterialModule } from "../../shared/material/material.module";
@NgModule({
    declarations: [StatisticsComponent],
    imports:[CommonModule,StatisticsRoutingModule,SharedModule,ChartModule,MaterialModule,ChartModule],
    exports:[],
    providers:[StatisticsService,DatePipe]
})
export class StatisticsModule{}