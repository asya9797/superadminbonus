import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { StatisticsService } from '../statistics.service';
import * as c3 from 'c3';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.scss',
    '../../../../../node_modules/c3/c3.min.css'],
  encapsulation: ViewEncapsulation.None

})
export class StatisticsComponent implements OnInit {
  public loading: boolean = false;
  isParliamentStatAvailable: boolean = false;
  isClientStatAvailable: boolean = false;
  public clientsCount: string = "";
  public dataOfParliaments: any;
  count = 0;
  count1 = 0;
  type1 = 'bar';
  data1;
  data2: any;
  clients: string = ""
  
  clientChart;
  options = {
    responsive: true,
    maintainAspectRatio: false,
  };

  constructor(private _statisticsService: StatisticsService, public datePipe: DatePipe) {
    this.data1 = {
      labels: [],
      datasets: [{
        label: 'Сумма',
        value:"Summa",
        backgroundColor: [
          'rgb(95,158,160)',
          'rgb(95,158,160)',
          'rgb(95,158,160)',
          'rgb(95,158,160)',
          'rgb(95,158,160)',
          'rgb(95,158,160)',
          'rgb(95,158,160)',
        ],
        data: [],
      }, {
        label: 'Бонус',
        value: "Bonus",
        backgroundColor: [
          'rgb(176,224,230)',
          'rgb(176,224,230)',
          'rgb(176,224,230)',
          'rgb(176,224,230)',
          'rgb(176,224,230)',
          'rgb(176,224,230)',
          'rgb(176,224,230)'
        ],
        data: [],
      },
      {
        label: 'Накопление',
        value: "Accumulation",
        backgroundColor: [
          'rgb(70,130,180)',
          'rgb(70,130,180)',
          'rgb(70,130,180)',
          'rgb(70,130,180)',
          'rgb(70,130,180)',
          'rgb(70,130,180)',
          'rgb(70,130,180)',
        ],
        data: [],
      },]
    };
    this.data2 = {
      labels: [],
      datasets: [
        {
          label: "Клиенты",
          value: "Clients",
          data: [],
          fill: true,
          borderColor: '#4bc0c0',
        }
      ]
    }
  }

  ngOnInit() {
    this.filterParliamentsByDateDays(this.count);
    this.filterClientsByDateDays(this.count1);
    this.clientCount();
    this.parliamentsData()

  }

  filterParliamentsByDateDays(count) {
    this.loading = true;
    this._statisticsService.filterParliamentsByDateDays(count).subscribe((data: any) => {
      let statData = data.data.reverse();
      this.data1 = new (<any>this.constructor().data1);
      this.setParliamentStatisticsInfo(statData, this.data1);
      this.loading = false;
    })
  }
  /**
   * 
   * @param statisticsInfo 
   * @param data 
   */
  setParliamentStatisticsInfo(statisticsInfo, data) {
    for (let i of statisticsInfo) {
      let time = this.datePipe.transform(new Date(i.Time.split('T')[0]), 'dd/MM/yyyy');
      data.labels.push(time);
      for (let j of data.datasets) {
        j.data.push(i[j.value]);
      }
    }
    this.isParliamentStatAvailable = true;
  }


  changeStatisticsWeekRight() {
    if (this.count > 0) {
      this.count--;
      this.filterParliamentsByDateDays(this.count)
    }

  }

  changeStatisticsWeekLeft() {
    this.count++;
    this.filterParliamentsByDateDays(this.count);
  }

  changeClientsWeekRight() {
    if (this.count1 > 0) {
      this.count1--;
      this.filterClientsByDateDays(this.count1)
    }
  }

  changeClientsWeekLeft() {
    this.count1++;
    this.filterClientsByDateDays(this.count1)
  }


  filterClientsByDateDays(count) {
    this.loading = true;
    this._statisticsService.filterClientByDateDays(count).subscribe((data: any) => {
      let statData = data.data.reverse();
      this.data2 = new (<any>this.constructor().data2);
      this.setClientStatisticsInfo(statData, this.data2);
      this.loading = false;
    })
  }

  setClientStatisticsInfo(statisticsInfo, data) {
    for (let i of statisticsInfo) {
      let time = this.datePipe.transform(i.RegTime, 'dd/MM/yyyy');
      data.labels.push(time);
      for (let j of data.datasets) {
        j.data.push(i.CountId);
      }
    }
    this.isClientStatAvailable = true;
  }

  clientCount() {
    this._statisticsService.getClientCount().subscribe((data: any) => {
      let count = data.data;
      this.clientsCount = count.toString();
      this.clients = this.clientsCount + "clients"
      return this.clients
    })
  }

  parliamentsData() {
    this._statisticsService.getParliaments().subscribe((data: any) => {
      this.dataOfParliaments = data.data;
    })
  }



}
