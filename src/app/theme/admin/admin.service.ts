import { Injectable } from "@angular/core";
import { ApiService } from "../../services/api.service";


@Injectable()
export class AdminService {
    
    constructor(private _apiService: ApiService) { }
    
    signUpSubAdmin(username, password, role) {
        let sendingData = { Username: username, Password: password, Role: role };
        return this._apiService.post("subAdmin/signUp", sendingData)
    }
    

    editSubAdminById(id,username,password,role){
        let sendingData = { Username: username, Password: password, Role: role }
        return this._apiService.put(`edit/subAdmin/${id}`,sendingData)
    }

    getAllAdmins(page,count){ 
        return this._apiService.get(`getAll/${page}/${count}`)
    }

    getAdmin(){
        return this._apiService.get(`get`)
    }
    deleteSubAdminById(id){
        return this._apiService.delete(`delete/subAdmin/${id}`)
    }
    getAdminsCount(){
        return this._apiService.get(`count`)
    }
   
}






