import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { AdminService } from '../admin.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AppService } from '../../../services/app.service';
import { MessageService } from 'primeng/components/common/messageservice';


@Component({
  selector: 'app-create-admin',
  templateUrl: './create-admin.component.html',
  styleUrls: ['./create-admin.component.scss']
})
export class CreateAdminComponent implements OnInit {
  public _matchingPasswordsError: string = undefined;
  public _matchingEditPasswordsError:string = undefined;
  public loading: boolean = false
  checkboxes: any = [
    {
      title: "i have bike",
      value: "i have bike",
      checked: false,
      adminRole: "role"
    },
    {
      title: "i have car",
      value: "i have car",
      checked: false,
      adminRole: "role"
    },
    {
      title: "i have moto",
      value: "i have moto",
      checked: false,
      adminRole: "role"
    },
    {
      title: "i have bike",
      value: "i have bike",
      checked: false,
      adminRole: "role"
    }
  ]
  arrForChecked = [];
  username: string = "";
  password: string = "";
  forCreateAndEdit: boolean = true;
  createAdminForm: FormGroup;
  checkboxGroup: any;
  passwordLabel: string = "New password:";
  incorrectPasswordMessage: boolean = false;
  editForm: FormGroup;



  constructor(private _appService: AppService, private _fb: FormBuilder, private _adminService: AdminService,private _messageService: MessageService,
    public matDialog: MatDialog,
    public dialogRef: MatDialogRef<CreateAdminComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }



  ngOnInit() {
    console.log("EDIT is ", this.data.edit);
    this._buildForm()
    // this.checkAdminInfo();
    this.checkInfo()
  }


  public onClickCreate() {
    // let accesses = [];
    // this.checkboxGroup.controls['checkBoxes'].controls.forEach((i, index) => {
    //   if (i.value) {
    //     accesses.push(this.checkboxes[index].value);
    //   }
    // })
    
    if (!this.data.edit) {
      this.loading = true
      this.createAdminForm.disable();
      //roln e i texy pti exnir accesses.join(',')
      this._adminService.signUpSubAdmin(this.createAdminForm.value.username, this.createAdminForm.value.newPassword, "roln e ").subscribe((response: any) => {
        this.loading = false;
      this.createAdminForm.enable();
      this._messageService.add({ severity: 'success', summary: '', detail: "Администратор был создан" });
        this.dialogRef.close(true);
      })
    }
    else {
      console.log("EDITING");
      this._appService.confirmDialog().subscribe((data) => {
        if (data && data.approve) {
          //roln e i texy pti exnir accesses.join(',')
          this._adminService.editSubAdminById(this.data.item.Id, this.editForm.value.username, this.editForm.value.newEditPassword, "roln e ").subscribe((data) => {
            this._messageService.add({ severity: 'success', summary: '', detail: "Данные администратора были обновление" });
            this.dialogRef.close(true);
          })
        }
      })
    }
  }

  onNoClick() {
    this.dialogRef.close();
  }

  // private checkAdminInfo() {

  //   if (this.data.edit) {
  //     let values = [];
  //     for (let i of this.checkboxes) {
  //       values.push(false);
  //     }
  //     this.editForm.patchValue({
  //       username: this.data.item.Username
  //     })

  //     let roles = this.data.item.Role.split(',');
  //     for (let i = 0; i < roles.length; i++) {
  //       for (let j = 0; j < this.checkboxes.length; j++) {
  //         if (roles[i] == this.checkboxes[j].value) {
  //           values[j] = true;
  //         }
  //       }
  //     }
  //     this.checkboxGroup.controls.checkBoxes.patchValue(values);
  //   }
  // }
  private _buildForm() {
    this.createAdminForm = new FormBuilder().group({
      username: ['', [Validators.required, Validators.maxLength(15), Validators.minLength(3)]],
      // adminRole: ['', Validators.required],
      //newPassword: ['', [Validators.required, Validators.pattern('((?=.*\d)(?=.*[a-zA-Z]).{4,20})'), Validators.minLength(4), Validators.maxLength(20)]],
      newPassword: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(15)]],
      repeatPassword: ['', Validators.required]
    }, { validator: this.matchingPasswords('newPassword', 'repeatPassword') })

    // this.checkboxGroup = new FormBuilder().group({
    //   checkBoxes: new FormBuilder().array([false, false, false, false])
    // })



    this.editForm = new FormBuilder().group({
      usernameEdit: ['', [Validators.required, Validators.maxLength(15), Validators.minLength(4)]],
      newEditPassword: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(20),]],
      repeatEditPassword: ['', Validators.required]
    }, { validator: this.matchingPasswords('newEditPassword', 'repeatEditPassword') })



    this.createAdminForm.valueChanges.subscribe((data) => {
      if (this.createAdminForm.value.newPassword != this.createAdminForm.value.repeatPassword && this.createAdminForm.get("newPassword").dirty && this.createAdminForm.get("repeatPassword").dirty) {
        this._matchingPasswordsError = "Пароли не совпадают"
      } else {
        this._matchingPasswordsError = undefined
      }
    })
    this.editForm.valueChanges.subscribe((data) => {
      if (this.editForm.value.newEditPassword != this.editForm.value.repeatEditPassword && this.editForm.get("newEditPassword").dirty && this.editForm.get("repeatEditPassword").dirty) {
        this._matchingEditPasswordsError = "Пароли не совпадают"
      } else {
        this._matchingEditPasswordsError = undefined
      }
    })



  }

  private matchingPasswords(passwordKey: string, confirmPasswordKey: string) {
    return (group: FormGroup): { [key: string]: any } => {
      let password = group.controls[passwordKey];
      let confirmPassword = group.controls[confirmPasswordKey];
      if (password.value !== confirmPassword.value) {
        return {
          mismatchedPasswords: true
        };
      }
    }
  }

  checkInfo() {
console.log(this.data)
    if (this.data.edit) {
      console.log(this.data);
      this.editForm.patchValue({
        usernameEdit: this.data.item.Username,
        

      })
     
    }
  }
}

