import { NgModule } from "@angular/core";
import { AdminComponent } from './admin/admin.component';
import { AdminRoutingModule } from "./admin-routing.module";
import { CommonModule } from "@angular/common";
import { AdminService } from "./admin.service";
import { MaterialModule } from '../../shared/material/material.module';
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { CookieService } from 'ngx-cookie-service';
import { HttpClientModule } from "@angular/common/http";
import { CreateAdminComponent } from './create-admin/create-admin.component';
import { SharedModule } from "../../shared/shared.module";



@NgModule({
   declarations:[AdminComponent, CreateAdminComponent],
   imports:[CommonModule,AdminRoutingModule,MaterialModule,FormsModule,ReactiveFormsModule,HttpClientModule,SharedModule],
   entryComponents: [CreateAdminComponent],
   providers:[AdminService,CookieService]
})
export class AdminModule{}