import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog } from '@angular/material';
import { AdminService } from '../admin.service';

import { CreateAdminComponent } from '../create-admin/create-admin.component';
import { MessageService } from 'primeng/components/common/messageservice';
import { AppService } from '../../../services/app.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  public adminsList: Array<object> = [];
  public totalCount: number = 0;
  public count: number = 10;
  public page: number = 0;
  public loading: boolean = false; 
  public forCheckAdmin: boolean = false;
  constructor(public dialog: MatDialog, private _adminService: AdminService,private _appService:AppService,private _messagesService: MessageService) { }

  ngOnInit() {
    this.getAllAdmins();
    this. getAdmin();
    this.getAdminsCount()
   
  }


  private getAllAdmins() {
    this.loading = true;
    this._adminService.getAllAdmins(this.page,this.count).subscribe((data:any) => {
      console.log("data is   ",data)
      this.adminsList = data.data;
      for(let i =0; i<=this.adminsList.length;i++){
           if(i != 0){
              this.forCheckAdmin = true;
           }
      }
      this.loading = false
    })
  }
  openDialog(): void {
    const dialogRef = this.dialog.open(CreateAdminComponent, {
      width: '450px',
      data: {
        edit: false
      },
      panelClass: "forPadding"

    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if(result){
        this.getAllAdmins();
      }
    });
  }

  openEditDialog(item): void {
    const editDialogRef = this.dialog.open(CreateAdminComponent, {
      width: '450px',
      data:{
        item:item,
        edit: true,
        create: false
      },
      panelClass: "forPadding"
    })
    editDialogRef.afterClosed().subscribe(result => {
      if(result){
        this.getAllAdmins();
      }
    })
  }
  getAdmin(){
         this._adminService.getAdmin().subscribe((data: any) => {
           console.log(data)
         })
  }
  onClickDelete(id): void{
    this._appService.confirmDialog().subscribe((data)=>{
      if(data && data.approve){
        this._messagesService.add({ severity: 'success', summary: '', detail: "Администратор был удален" });
        this.deleteAdminById(id);
      }
    })
  }

  deleteAdminById(id){
    this._adminService.deleteSubAdminById(id).subscribe((data)=>{
      this.getAllAdmins();
    })
  }
  getAdminsCount(){
    this._adminService.getAdminsCount().subscribe((data:any)=>{
      this.totalCount = data.data;
      console.log(this.totalCount)
    })
  }
  public paginate(event){
    this.count = event.pageSize;
    this.page = event.pageIndex;
    this._adminService.getAllAdmins(this.page,this.count).subscribe((data:any) => {
      console.log("data is   ",data)

      this.adminsList = data.data;
      
    })
    }
   
  }




