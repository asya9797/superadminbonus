import { Component, OnInit, Inject, Input } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AdminService } from '../admin.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from '../../../services/api.service';
import { MessageService } from 'primeng/components/common/messageservice';
@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent implements OnInit {
  public confirmForm: FormGroup;
  public loading: boolean = false;
  public enteredIncorrect:boolean = false;
  constructor( public dialogRef: MatDialogRef<ConfirmComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private _apiService: ApiService, private _messageService: MessageService ) { }


  ngOnInit() {
    this._buildForm()
  }

  onNoClick() {
    this.dialogRef.close({ approve: false });
  }


  private _buildForm() {
    this.confirmForm = new FormBuilder().group({
      confirmPassword: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(20)]],
    })
  }

  public onConfirmClick() {
    this.loading = true;
    this.confirmForm.disable();
    this._apiService.checkPassword({
      Password: this.confirmForm.value.confirmPassword,
    }).subscribe((response: any) => {
      console.log("response is ----",response)
      if (response.status) {
        this.loading = false;
       this.confirmForm.enable();
        this.dialogRef.close({ approve: true })
      }
    },(err) => { 
      this.loading = false;
       this.confirmForm.enable();
      if(err.status == 404){
          this.enteredIncorrect = true;
      }
      if(err.status == 400){
        this._messageService.add({severity:'error', summary: '', detail:"Пароль введен неверно"});
      }
    })
  }
}
