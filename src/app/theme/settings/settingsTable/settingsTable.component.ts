import { Component, Input, Output, EventEmitter } from "@angular/core";
import { AddComponent } from '../add/add.component';
import { AppService } from '../../../services/app.service';

@Component({
    selector: 'app-settings-table',
    templateUrl: './settingsTable.component.html',
    styleUrls: ['./settingsTable.component.scss']
})
export class SettingsTableComponent {
    @Input() data: Array<object> = [];
    @Input() type: string = undefined;
    @Output() delete: EventEmitter<number> = new EventEmitter<number>();
    constructor(){}
    ngOnInit(){
        setTimeout(() => console.log(this.data), 5000)
    }

    onClickDelete(id) {
        this.delete.emit(id);
    }
    setImage(item) {
        if (this.type && this.type != 'currency') {
            let url = '/assets/def.png';
            if (item.Image) {
                //url = 'http://46.101.179.50:3000/static/' + item.Image
                url = 'http://46.101.179.50:3000/static/' + item.Image
            }
            let styles = {
                'background-image': 'url(' + url + ')'
            }
            return styles;
        }
    }

}