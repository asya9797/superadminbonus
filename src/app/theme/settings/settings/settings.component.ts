import { Component, OnInit, Inject } from '@angular/core';
import { SettingsService } from '../settings.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialog, } from '@angular/material';
import { AddComponent } from '../add/add.component';
import { AppService } from '../../../services/app.service';



import {MessageService} from 'primeng/components/common/messageservice';
import { EditPercentComponent } from '../edit-percent/edit-percent.component';
@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent implements OnInit {

  public _matchingPasswordsError: string = undefined;
  public loginForm: FormGroup;
  public passwordForm: FormGroup;
  public country: any;
  public imageUrl: string = "http://46.101.179.50:3000/static/"
  public panelOpenState = false;
  public language: any;
  public currency: any;
  public loading: boolean = false;
  public loadingPassword:boolean = false;
  public configData: any;
  constructor(private _settingsService: SettingsService, private _appService: AppService, private _messageService: MessageService,public dialog: MatDialog,) { }

  ngOnInit() {
    this._buildForm();
    this.getCountries();
    this.getLanguages();
    this.getCurrencies()
    this.configs();
   
  }


  public _changeAdminPassword(): void {
    this._settingsService.oldPassword = this.passwordForm.value.oldPassword;
    this._settingsService.newPassword = this.passwordForm.value.newPassword;
    this.loadingPassword= true;
    this.passwordForm.disable();
    this._settingsService.changePassword().subscribe((response: any) => {
      if (response.status && response.data) {
        this.loadingPassword = false;
      this.passwordForm.enable();
        this._messageService.add({severity:'success', summary:'', detail: "Пароль был изменен"});
      }
      
    },err => {
      this.loadingPassword= false;
      this.passwordForm.enable();
      this._messageService.add({severity:'error', summary: '', detail: "Какое-то поле заполнено неправильно"});
    
  })
  }
  public _changeUsername() {
   
    this._settingsService.newUsername = this.loginForm.value.newLogin;
    this.loading = true;
    this.loginForm.disable();
    this._settingsService.changeUsername().subscribe((response: any) => {
      if (response.status && response.data){
        console.log(response)
        this.loading = false;
        this.loginForm.enable();
      this._messageService.add({severity:'success', summary:'', detail: "Логин был изменен "});
      }
      
    } ,err => {
      this.loading = false;
      this._settingsService.newUsername = "";
      this.loginForm.enable();
        this._messageService.add({severity:'error', summary: '', detail: "Это поле заполнено неправильно"});
      
    })
  
  }
  public _buildForm() {
    this.passwordForm = new FormBuilder().group({
      oldPassword: ['', Validators.required],
      newPassword: ['', [Validators.required, Validators.pattern('((?=.*\d)(?=.*[a-zA-Z]).{4,20})'), Validators.minLength(3), Validators.maxLength(15)]],
      repeatPassword: ['', Validators.required]
    },{ validator: this.matchingPasswords('newPassword', 'repeatPassword') })
    this.loginForm = new FormBuilder().group({
      newLogin: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(15)]],
    })


    this.passwordForm.valueChanges.subscribe((data) => {
      if (this.passwordForm.value.newPassword != this.passwordForm.value.repeatPassword && this.passwordForm.get("newPassword").dirty && this.passwordForm.get("repeatPassword").dirty) {
        this._matchingPasswordsError = "Пароли не совпадают"
      } else {
        this._matchingPasswordsError = undefined
      }
    })
  }

  private matchingPasswords(passwordKey: string, confirmPasswordKey: string) {
    return (group: FormGroup): { [key: string]: any } => {
      let password = group.controls[passwordKey];
      let confirmPassword = group.controls[confirmPasswordKey];
      if (password.value !== confirmPassword.value) {
        return {
          mismatchedPasswords: true
        };
      }
    }
  }

  


  getCountries() {
    this.loading = true;
    this._settingsService.getCountries().subscribe((data: any) => {
      console.log("countries-------", data)
      this.country = data.data;
      this.loading = false;
    })
  }


  onClickDelete(event): void {
    console.log(event)
    this._appService.confirmDialog().subscribe((data) => {
      if (data && data.approve) {
        if (event.type == 'country') {
          this.deleteCountry(event.id);
          this._messageService.add({severity:'success', summary:'', detail:"Страна удалена"});
        }
        if (event.type == 'language') {
          this.deleteLanguages(event.id);
          this._messageService.add({severity:'success', summary:'', detail:"Язык удален"});
        }

        if (event.type == 'currency') {
          this.deleteCurrency(event.id);
          this._messageService.add({severity:'success', summary:'', detail:"Валюта удалена"});
        }
      }
    })
  }
  deleteCountry(id) {
    this._settingsService.deleteCountry(id).subscribe((data: any) => {
      console.log("deleted country", data)
      this.getCountries()
    })
  }
  getLanguages() {
    this.loading = true;
    this._settingsService.getLanguages().subscribe((data: any) => {
      console.log("language----", data)
      this.language = data.data;
      this.loading = false;
    })
  }

  changedData(event) {
    if (event == 'country') {
      this.getCountries();
      
    } else if (event == 'language') {
      this.getLanguages(); 
     
    } else if (event == 'currency') {
      this.getCurrencies();
     
    }
  }
  deleteLanguages(id) {
    this._settingsService.deleteLanguage(id).subscribe((data: any) => {
      console.log('deleted language', data)
      this.getLanguages();
    })
  }
  getCurrencies() {
    this.loading = true;
    this._settingsService.getCurrencies().subscribe((data: any) => {
      console.log("currency----", data)
      this.currency = data.data;
      this.loading = false;
      
    })
  }
  deleteCurrency(id) {
    this._settingsService.deleteCurrency(id).subscribe((data: any) => {
      console.log("deleted currency", data)
      this.getCurrencies();
    })
  }
  configs(){
    this._settingsService.getConfig().subscribe((data:any) =>{
      console.log(data);
      this.configData = data.data[0]
    })
  }

  openDialog(data): void {
    const dialogRef = this.dialog.open(EditPercentComponent,{
      width: '400px',
      data: data,
      panelClass: "forPadding",
      
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if(result){
        this.configs()
      }
    });
}
  



}
