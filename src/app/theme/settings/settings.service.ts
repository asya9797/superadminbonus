import { Injectable } from "@angular/core";
import { ApiService } from "../../services/api.service";
@Injectable()
export class SettingsService {
    public oldPassword: string = "";
    public newPassword: string = "";
    public newUsername: string = "";
    public adminRole: string = "";
    
    constructor(private _apiService: ApiService) { }
    changePassword() {
        let sendingData = {OldPassword: this.oldPassword, Password: this.newPassword}
        return this._apiService.put("change/password/yourself",sendingData )
    }
    changeUsername(){
        let sendingData = {Username: this.newUsername}
        return this._apiService.put("edit/yourself",sendingData)
    }
    getCountries(){
        return this._apiService.get("getAll/countries")
    }
    createCountryImage(id,body){
        return this._apiService.postImage(`create/countryImage/${id}`,body)
    }
    signUpCountry(body){
        return this._apiService.post(`signUp/country`,body)
    }
    deleteCountry(id){
        return this._apiService.delete(`delete/country/${id}`)
    }
    getLanguages(){
        return this._apiService.get(`getAll/languages`)
    }
    signUpLanguage(body){
        return this._apiService.post(`signUp/language`,body)
    }
    createLanguageImage(id,body){
            return this._apiService.postImage(`create/languageImage/${id}`,body)
    }
    getCurrencies(){
        return this._apiService.get(`getAll/currencies`)
    }
    signUpCurrency(body){
        return this._apiService.post(`signUp/currency`,body)
    }
    deleteLanguage(id){
        return this._apiService.delete(`delete/language/${id}`)
    }
    deleteCurrency(id){
        return this._apiService.delete(`delete/currency/${id}`)
    }
    getConfig(){
        return this._apiService.get("config")
    }
    creditPercent(body){
        return this._apiService.put("config/CreditPercent",body)
    }
    companyPercent(body){
        return this._apiService.put("config/CompanyPercent",body)
    }
    kassaPercent(body){
        return this._apiService.put("config/KassaPercent",body)
    }
    clientActiveBonus(body){
        return this._apiService.put("config/ClientActiveBonusPercent",body)
    }
}