import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SettingsService } from '../settings.service';
import { MessageService } from 'primeng/components/common/messageservice';
@Component({
  selector: 'app-edit-percent',
  templateUrl: './edit-percent.component.html',
  styleUrls: ['./edit-percent.component.scss']
})
export class EditPercentComponent implements OnInit {
  public percentForm: FormGroup;
  public loading: boolean = false;
  public forNegativeCode: string = undefined;
  public percentSize: string = undefined;
  constructor(
    public dialogRef: MatDialogRef<EditPercentComponent>,

    @Inject(MAT_DIALOG_DATA) public data: any,private _settingsService: SettingsService,private _messageService: MessageService) { }

  ngOnInit() {
    this._buildForm()
  }
  onNoClick() {
    this.dialogRef.close();
  }

  private _buildForm() {
    this.percentForm = new FormBuilder().group({
      percentType: ['', Validators.required]
    } )
    this.percentForm.valueChanges.subscribe((data) => {
    if((this.data == 'credit' && this.percentForm.value.percentType >= 100) || (this.data == 'company' && this.percentForm.value.percentType >= 100) || (this.data == 'kassa' && this.percentForm.value.percentType >= 100)){
      this.percentSize = "Введенный номер должен быть от 1 до 100";
    }else{
      this.percentSize = undefined;
      console.log(this.percentForm.get('percentType').value);
      
    }
    })
  }
  creditPercent(){
    let sendingData = {CreditProcent: +this.percentForm.value.percentType}
    this._settingsService.creditPercent(sendingData).subscribe((data:any) => {
      this.dialogRef.close(true);
      this._messageService.add({ severity: 'success', summary: '', detail: "Редактирован" });
    })
  }
  companyPercent(){
    let sendingData = {CompanyPrecent: +this.percentForm.value.percentType}
        this._settingsService.companyPercent(sendingData).subscribe((data:any) => {
          this.dialogRef.close(true);
          this._messageService.add({ severity: 'success', summary: '', detail: "Редактирован" });
        })
  }
  kassaPercent(){
    let sendingData = {KassaProcent: +this.percentForm.value.percentType}
    this._settingsService.kassaPercent(sendingData).subscribe((data:any) => {
      this.dialogRef.close(true);
      this._messageService.add({ severity: 'success', summary: '', detail: "Редактирован" });
    })
  }
  clientActiveBonus(){
    let sendingData = {ClentActiveBonusPercent: this.percentForm.value.percentType}
    this._settingsService.clientActiveBonus(sendingData).subscribe((data:any) => {
      this.dialogRef.close(true);
      this._messageService.add({ severity: 'success', summary: '', detail: "Редактирован" });
    })
  }

  putPercent(){
    this.loading = true;
    this.percentForm.disable();
    if(this.data == 'credit'){
      this.creditPercent()
    }else if(this.data == 'company'){
      this.companyPercent()
    }else if(this.data == 'kassa'){
      this.kassaPercent()
    }else if(this.data = 'summa'){
      this.clientActiveBonus()
    }
  }

}
