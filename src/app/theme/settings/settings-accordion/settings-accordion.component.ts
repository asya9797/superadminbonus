import { Component, OnInit, Input,Output,EventEmitter} from '@angular/core';
import { MatDialog } from '@angular/material';
import { AddComponent } from '../add/add.component';
import { SettingsService } from '../settings.service';


@Component({
  selector: 'app-settings-accordion',
  templateUrl: './settings-accordion.component.html',
  styleUrls: ['./settings-accordion.component.scss']
})
export class SettingsAccordionComponent implements OnInit {
  @Input() type:string;
  @Input() data: any;
  @Input() tabledata: Array<object> = [];
  @Output() deleteEvent: EventEmitter<object> = new EventEmitter<object>();
  @Output() changed= new EventEmitter<string>();
  @Input() loading: boolean;
  public panelOpenState = false;
  public language: any;
  public country: any;
  constructor(public dialog: MatDialog,private _settingsService: SettingsService) { }

  ngOnInit() {
  }
  openDialog(data): void {
    const dialogRef = this.dialog.open(AddComponent, {
      width: '400px',
      data: data,
      panelClass: "forPadding",
      
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if(result){
        this.changed.emit(this.type);
      }
    });
}

deleted(event){
  this.deleteEvent.emit({id:event,type:this.type})
}

}
