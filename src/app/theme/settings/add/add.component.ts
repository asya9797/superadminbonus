import { Component, OnInit, Inject, Output, EventEmitter, Input } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SettingsService } from '../settings.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MessageService } from 'primeng/components/common/messageservice';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {
  private _image: Event;
  public localImage: string = "/assets/def.png";
  public countryName: string = "";
  public addForm: FormGroup;
  public countryId: number;
  public languageId: number;
  public chooseImage: boolean = false;
  public loading: boolean = false;

  constructor(public dialogRef: MatDialogRef<AddComponent>,

    @Inject(MAT_DIALOG_DATA) public data: any, private _settingsService: SettingsService, private _messageService: MessageService) { }

  ngOnInit() {
    this._buildForm((this.data.forAdd == 'currency') ? false : true);
    this.getCountries();


  }
  onNoClick() {
    this.dialogRef.close();
  }
  private uploadImageCountry(id, event) {
    let formData = new FormData();
    if (event && event.target) {
      let fileList: FileList = event.target.files;
      if (fileList.length > 0) {
        let file: File = fileList[0];
        formData.append('country', file, file.name);
        this._settingsService.createCountryImage(id, formData).subscribe((data: any) => {
          this.loading = false;
          this.dialogRef.close(true);
        })
      }
    }

  }
  private uploadImageLanguage(id, event) {
    let formData = new FormData();
    if (event && event.target) {
      let fileList: FileList = event.target.files;
      if (fileList.length > 0) {
        let file: File = fileList[0];
        formData.append('language', file, file.name);
        this._settingsService.createLanguageImage(id, formData).subscribe((data: any) => {
          this.loading = false;
          this.dialogRef.close(true);
        })
      }
    } else {
      this.chooseImage = true;
    }

  }
  photoUpload(event) {
    if (event) {
      let reader = new FileReader()
      this._image = event;
      reader.onload = (e: any) => {
        this.localImage = e.target.result;
        console.log(this.localImage)
      };
      reader.readAsDataURL(event.target.files[0]);

    }

  }
  createNewCountry() {
    this._settingsService.signUpCountry({ Name: this.addForm.value.name }).subscribe((data: any) => {
      console.log("upload country name", data)
      this.countryId = data.data.Id;
      this.uploadImageCountry(this.countryId, this._image);
      this._messageService.add({ severity: 'success', summary: '', detail: "Country created" });
    })
  }

  private _buildForm(symbolDisabled: boolean) {
    this.addForm = new FormBuilder().group({
      name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(30)]],
      chooseImg: ['', Validators.required],
      symbol: [{ value: '', disabled: symbolDisabled }, [Validators.required, Validators.maxLength(1)]]
    }, )
    if (!symbolDisabled) { this.addForm.get("chooseImg").patchValue("5645645") }
  }
  getCountries() {
    this._settingsService.getCountries().subscribe((data: any) => {
      console.log("new countries----", data)

    })
  }
  createNewLanguage() {
    this._settingsService.signUpLanguage({ Name: this.addForm.value.name }).subscribe((data: any) => {
      this.languageId = data.data[0].Id;
      this.uploadImageLanguage(this.languageId, this._image);
      this._messageService.add({ severity: 'success', summary: '', detail: "Language created" });

    })
  }
  createNewCurrency() {
    this._settingsService.signUpCurrency({ Name: this.addForm.value.name, Symbol: this.addForm.value.symbol }).subscribe((data: any) => {
      this.loading = false;
      this.dialogRef.close(true);
      this._messageService.add({ severity: 'success', summary: '', detail: "Currency created" });
    })
  }

  create() {
    this.loading = true;
    this.addForm.disable();
    if (this.data.forAdd == 'language') {
      this.createNewLanguage();
    } else if (this.data.forAdd == 'country') {
      this.createNewCountry();
    } else if (this.data.forAdd == 'currency') {
      this.createNewCurrency();
    }
  }


}
