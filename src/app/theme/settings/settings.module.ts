import { NgModule } from "@angular/core";
import { SettingsRoutingModule } from "./settings-routing.module";
import { SettingsComponent } from "./settings/settings.component";
import { SettingsService } from "./settings.service";
import { MaterialModule } from '../../shared/material/material.module';
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { CookieService } from 'ngx-cookie-service';
import { HttpClientModule } from "@angular/common/http";
import { CommonModule } from "@angular/common";
import { AddComponent } from './add/add.component';
import { SettingsTableComponent } from "./settingsTable/settingsTable.component";
import { SettingsAccordionComponent } from './settings-accordion/settings-accordion.component';
import { SharedModule } from "../../shared/shared.module";
import { EditPercentComponent } from './edit-percent/edit-percent.component';



@NgModule({
    declarations:[SettingsComponent, AddComponent,SettingsTableComponent, SettingsAccordionComponent, EditPercentComponent],
    imports:[SettingsRoutingModule,MaterialModule,ReactiveFormsModule, FormsModule,HttpClientModule,CommonModule,SharedModule,],
    exports:[],
    entryComponents: [AddComponent,EditPercentComponent],
    providers:[SettingsService,CookieService]
})
export class SettingsModule{}