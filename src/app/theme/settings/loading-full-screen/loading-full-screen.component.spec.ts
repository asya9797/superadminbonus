import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadingFullScreenComponent } from './loading-full-screen.component';

describe('LoadingFullScreenComponent', () => {
  let component: LoadingFullScreenComponent;
  let fixture: ComponentFixture<LoadingFullScreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadingFullScreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadingFullScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
