import { Component, OnInit, Input } from '@angular/core';
import { ProviderService } from '../provider.service';
import { DatePipe } from '@angular/common';
import { MatDialog } from '@angular/material';
import { CreateProviderComponent } from '../create-provider/create-provider.component';
import { AppService } from '../../../services/app.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
@Component({
  selector: 'app-provider',
  templateUrl: './provider.component.html',
  styleUrls: ['./provider.component.scss']
})
export class ProviderComponent implements OnInit {
  public loading: boolean = false;
  public count: number = 10;
  public page: number = 0;
  public providerInfo: any;
  public totalCount: number = 0;
  public isProvidersAvailable: boolean = false;
  constructor(private _providerService: ProviderService, private _datePipe: DatePipe, public dialog: MatDialog, private _appService: AppService, private _messageService: MessageService) { }

  ngOnInit() {
    this._getProvidersData()
  }
  getAllProviders() {
    return  this._providerService.getAllProviders(this.page, this.count).map((data: any) => {
      console.log("provider data", data)
      this.providerInfo = data.data
      for (let i of this.providerInfo) {
        i["parliament"] = [];
        i["open"] = false;
      }
      return data;
    })
    .catch((error) => {
      console.log("err ---------------- ", error);
      return Observable.of(false);
    })
  }
  // getProviderInfo(id) {
  //   this._providerService.getProviderInfo(this.page,this.count,id).subscribe((data: any) => {
  //     console.log("provider info", data)
  //     for (let i of data.data) {
  //       let time = i.RegTime;
  //       i["RegTime"] = this._datePipe.transform(time, 'dd-MM-yyyy');
  //     }
  //     this.providerInfo = data.data;
  //     console.log("new prov info", this.providerInfo);
  //   })
  // }

  private _providersCount() {
    return this._providerService.getProvidersCount().map((data: any) => {
      this.totalCount = data.data;
      this.isProvidersAvailable = true;
      console.log(this.totalCount)
      return data;
    })
  }
  private _getFilterProviderCount(id){
      this._providerService.getProvidersInfoCount(id).subscribe((data: any) => {
        this.totalCount = data.data;
      })
  }
  providersInfo(id){
    this._providerService.getProviderInfo(this.page,this.count,id).subscribe((data: any) => {

    })
  }


  private _getProvidersData() {
    this.loading = true;
    const combined = Observable.forkJoin(
      this._providersCount(),
      this.getAllProviders()
    )
    combined.subscribe((data) => {
      console.log(data);
      this.loading = false;
    })
  }

  public paginate(event) {
    this.loading = true;
    this.count = event.pageSize;
    this.page = event.pageIndex;
    
    return this._providerService.getAllProviders(this.page, this.count).map((data: any) => {
      console.log("provider data", data)
      this.providerInfo = data.data;
      for (let i of this.providerInfo) {
        i["bar"] = []
      }
    }).subscribe((data) => {
      console.log(data);
      this.loading = false;
    })
  }

  openDialog(item): void {
    const dialogRef = this.dialog.open(CreateProviderComponent, {
      width: '450px',
      panelClass: "forPadding",
      data: item
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if(result){
        this._getProvidersData();
      }
    });
  }

  getStyle(item) {
    if (item.debt < 0) {

      return {
        'color': 'red'
      }
    } else {
      return { 'color': 'green' }
    }
  }

  // provideri debt-y reset enelu hamar e

  resetDebt(id) {
    this._providerService.resetDebt(id, {}).subscribe((data: any) => {
      this._getProvidersData();
      this._messageService.add({ severity: 'success', summary: '', detail: "Долг обнулен" })
    }, (err) => {
      if (err.error.data.message == "parliament not changed, because your sending data is already exists in db") {
        this._messageService.add({ severity: 'error', summary: '', detail: "Парламент не изменился, поскольку вы отправляете данные уже в базе данных" })
      }
    })
  }

  onClickDebtEdit(id) {
    this._appService.confirmDialog().subscribe((data) => {
      if (data && data.approve) {
        this.resetDebt(id);
      }
    })

  }

  clickToggle(id,isActive) {
    this.loading= true;
    isActive = !isActive;
    if(isActive){
      isActive = 1;
    }else{
      isActive = 0;
    }
    return this._providerService.updateProvider(id,{IsActive: isActive}).map((data:any) => {
      console.log("is activey poxvav",data.data)
    }).subscribe((data) => {
      console.log(data)
      this.loading = false;
      this._getProvidersData();
    })
}


deleteProviderById(id) {
  this._providerService.deleteProvider(id).subscribe((data: any) => {
    this._getProvidersData();
    this._messageService.add({ severity: 'success', summary: '', detail: "Провайдер удален" })
  })
}

onClickDelete(id): void {
  this._appService.confirmDialog().subscribe((data) => {
    if (data && data.approve) {
      this.deleteProviderById(id);
    }
  })
}
}
