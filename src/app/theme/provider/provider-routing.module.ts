import { NgModule } from "@angular/core";
import { Routes,RouterModule } from "@angular/router";
import { ProviderComponent } from "./provider/provider.component";
const providerRoutes:Routes = [
    {
        path:"",
        component: ProviderComponent
    }
]
@NgModule({
    imports:[RouterModule.forChild(providerRoutes)],
    exports: [RouterModule]
})
export class ProviderRoutingModule{

}