import { Injectable } from "@angular/core";
import { ApiService } from "../../services/api.service";

@Injectable()
export class ProviderService{
   constructor(private _apiService: ApiService){}

   getAllProviders(page,count){
       return this._apiService.get(`getAll/providers/${page}/${count}`);
   }
   getProviderInfo(page,count,id){
       return this._apiService.get(`provider/info/${page}/${count}/${id}`)
   }
   getProvidersCount(){
       return this._apiService.get('provider/count')
   }
   resetDebt(id,body){
       return this._apiService.put(`provider/resetDebt/${id}`,body)
   }
   signUpProvider(body){
       return this._apiService.post(`signUp/provider`,body)
   }
   getProvidersInfoCount(id){
       return this._apiService.get(`provider/count/${id}`)
   }
   updateProvider(id,body){
       return this._apiService.put(`provider/isActive/${id}`,body)
   }
   deleteProvider(id){
       return this._apiService.delete(`provider/${id}`)
   }
  
}