import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ProviderService } from '../provider.service';
import { MessageService } from 'primeng/components/common/messageservice';
@Component({
  selector: 'app-create-provider',
  templateUrl: './create-provider.component.html',
  styleUrls: ['./create-provider.component.scss']
})
export class CreateProviderComponent implements OnInit {
  public createProviderForm: FormGroup;
  public _matchingPasswordsError: string = undefined;
  public loading: boolean = false;
  public loginMessage: string = undefined;
  constructor(public dialogRef: MatDialogRef<CreateProviderComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private _providerService: ProviderService, private _messageService: MessageService) { }

  ngOnInit() {
    this._buildForm()
  }
  onNoClick() {
    this.dialogRef.close();
  }


  private _buildForm() {
    this.createProviderForm = new FormBuilder().group({
      name: ['', [Validators.required, Validators.maxLength(15), Validators.minLength(3)]],
      surname: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(15)]],
      username: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(15)]],
      password: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(15)]],
      repeatPassword: ['', Validators.required]
    }, { validator: this.matchingPasswords('password', 'repeatPassword') })

    this.createProviderForm.valueChanges.subscribe((data) => {
      if (this.createProviderForm.value.password != this.createProviderForm.value.repeatPassword && this.createProviderForm.get("password").dirty && this.createProviderForm.get("repeatPassword").dirty) {
        this._matchingPasswordsError = "Пароли не совпадают"
      } else {
        this._matchingPasswordsError = undefined
      }
    })

  }
  private matchingPasswords(passwordKey: string, confirmPasswordKey: string) {
    return (group: FormGroup): { [key: string]: any } => {
      let password = group.controls[passwordKey];
      let confirmPassword = group.controls[confirmPasswordKey];
      if (password.value !== confirmPassword.value) {
        return {
          mismatchedPasswords: true
        };
      }
    }
  }



  createProvider() {
   
    let sendingData = {
      Name: this.createProviderForm.value.name,
      Surname: this.createProviderForm.value.surname,
      Username: this.createProviderForm.value.username,
      Password: this.createProviderForm.value.password,
    }
    this.loading = true;
    this.createProviderForm.disable();
    
   
    this._providerService.signUpProvider(sendingData).subscribe((data: any) => {
      console.log("sending dataaa", data.data)
      this.loading = false;
      this.createProviderForm.enable();
      this._messageService.add({ severity: 'success', summary: '', detail: "Провайдер был создан" });
      this.dialogRef.close(true);
    },(err) => {
      this.loginMessage = err.error.text;
      this.loading = false;
      this.createProviderForm.enable();
    })
  }


}
