import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ProviderService } from "./provider.service";
import { ProviderRoutingModule } from "./provider-routing.module";
import { ProviderComponent } from "./provider/provider.component";
import { CollapsibleModule } from "angular2-collapsible";
import { MaterialModule } from "../../shared/material/material.module";
import { CreateProviderComponent } from './create-provider/create-provider.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ParliamentsOfProviderComponent } from './parliaments-of-provider/parliaments-of-provider.component';
import { SharedModule } from "../../shared/shared.module";
@NgModule({
    declarations: [ProviderComponent, CreateProviderComponent, ParliamentsOfProviderComponent],
    imports: [CommonModule,ProviderRoutingModule,CollapsibleModule,MaterialModule,FormsModule,ReactiveFormsModule,SharedModule],
    exports:[],
    entryComponents: [CreateProviderComponent],
    providers:[ProviderService]

})
export  class ProviderModule{
}