import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParliamentsOfProviderComponent } from './parliaments-of-provider.component';

describe('ParliamentsOfProviderComponent', () => {
  let component: ParliamentsOfProviderComponent;
  let fixture: ComponentFixture<ParliamentsOfProviderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParliamentsOfProviderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParliamentsOfProviderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
