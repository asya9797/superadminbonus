import { Component, OnInit, Input } from '@angular/core';
import { ProviderService } from '../provider.service';
import { DatePipe } from '@angular/common';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
@Component({
  selector: 'app-parliaments-of-provider',
  templateUrl: './parliaments-of-provider.component.html',
  styleUrls: ['./parliaments-of-provider.component.scss']
})
export class ParliamentsOfProviderComponent implements OnInit {
  public _open:boolean = false;
  public loading: boolean = false;
  public page: number = 0;
  public count: number = 5;
  public totalCount :number = 0;
  public info: any;
  public isParliamentsAvailable: boolean = false;
  @Input() provider: Array<Object>;
  @Input() providerId: number;
  @Input() providerInfo: Array<Object>;
  @Input('open')
         set open(value: boolean){
           this._open = value;
           if(this._open){
             this._getParliamentsOfProviderData(this.providerId)
           }
           
         }

  constructor( private _providerService: ProviderService, private _datePipe: DatePipe) { }

  ngOnInit() {
  }

  getProviderInfo(id) {
    return this._providerService.getProviderInfo( id,this.page,this.count).map((data: any) => {
      for (let i of data.data) {
        let time = i.RegTime;
        i["RegTime"] = this._datePipe.transform(time, 'dd-MM-yyyy');
      }
      if (data.status) {
        for (let i of this.providerInfo) {
          if (i['Id']== id) {
            i["parliament"] = data.data
          }
        }
      }
    return data;
    })
  }
  getParlCount(id){
    return this._providerService.getProvidersInfoCount(id).map((data:any) => {
      this.totalCount = data.data;
      this.isParliamentsAvailable = true;
      return data;
    })
  }


  private _getParliamentsOfProviderData(id) {
    this.loading = true;
    const combined = Observable.forkJoin(
      this.getParlCount(id),
      this.getProviderInfo(id)
    )
    combined.subscribe((data) => {
      console.log(data);
      this.loading = false;
    })
  }
  public paginate(event,id) {
    this.count = event.pageSize;
    this.page = event.pageIndex;
    this.loading = true;
    return this._providerService.getProviderInfo(this.providerId,this.page, this.count).map((data: any) => {
      for (let i of data.data) {
        let time = i.RegTime;
        i["RegTime"] = this._datePipe.transform(time, 'dd-MM-yyyy');
      }
      if (data.status) {
        for (let i of this.providerInfo) {
          if (i['Id']== id) {
            i["parliament"] = data.data
          }
        }
      }
    }).subscribe((data) => {
      console.log(data);
      this.loading = false;
    })
  }

}
