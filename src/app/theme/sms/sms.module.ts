import { NgModule } from "../../../../node_modules/@angular/core";
import { SmsComponent } from "./sms/sms.component";
import { SmsService } from "./sms.service";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MaterialModule } from "../../shared/material/material.module";
import { SmsRoutingModule } from "./sms-routing.module";
import { CookieService } from "ngx-cookie-service";
import { ViewTextComponent } from "../annauncement/view-text/view-text.component";

@NgModule({
    declarations: [SmsComponent],
    imports:[CommonModule,FormsModule,MaterialModule,SmsRoutingModule,ReactiveFormsModule],
    exports: [],
    providers:[SmsService,CookieService]
    
})
export class SmsModule {
    
}