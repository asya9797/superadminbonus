import { Component, OnInit } from '@angular/core';
import { SmsService } from '../sms.service';
import { DatePipe } from '@angular/common';
import { AppService } from '../../../services/app.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { ViewTextComponent } from '../../annauncement/view-text/view-text.component';
@Component({
  selector: 'app-sms',
  templateUrl: './sms.component.html',
  styleUrls: ['./sms.component.scss']
})
export class SmsComponent implements OnInit {
  public totalCount:number = 0;
  public loading:boolean = false;
  public page:number = 0;
  public count: number = 5;
  public smsInfo: any;
  public sms: boolean = false;
  constructor(private _smsService:SmsService, private _datePipe: DatePipe, private _appService: AppService, private _messageService: MessageService, public dialog: MatDialog) { }

  ngOnInit() {
    this.smsCount();
    this.getSms()
  }
  
  smsCount(){
    this._smsService.getSmsCount().subscribe((data:any) => {
       this.totalCount = data.data;
       console.log(this.totalCount)
    })
  }
  getSms(){
    console.log('sdshfhsfhs')
    this.loading = true;
    this._smsService.getAllSms(this.page,this.count).subscribe((data:any) => {
      for (let i of data.data) {
        let time = i.Time;
        i["Time"] = this._datePipe.transform(time, 'dd-MM-yyyy');
      }
        this.smsInfo = data.data;
        this.loading = false;
        console.log(this.smsInfo)
    })
  }
  public paginate(event){
    this.count = event.pageSize;
    this.page = event.pageIndex;
    this.loading = true;
     this._smsService.getAllSms( this.page, this.count).subscribe((data: any) => {
      for (let i of data.data) {
        let time = i.Time;
        i["Time"] = this._datePipe.transform(time, 'dd-MM-yyyy');
      }
        this.smsInfo = data.data;
        this.loading = false;
      })
    }
    

    onClickDelete(id): void {
      this._appService.confirmDialog().subscribe((data) => {
        if (data && data.approve) {
          this.deleteSmsById(id)
        }
      })
    }
  
    deleteSmsById(id) {
      this._smsService.deleteSms(id).subscribe((data: any) => {
        console.log("deleted");
        this.getSms();
        this.smsCount()
        this._messageService.add({ severity: 'success', summary: '', detail: "Этот сообщение был удален" })
      })
    }

    openDialog(item,bool): void {
      const dialogRef = this.dialog.open(ViewTextComponent, {
        width: '500px',
        data: {
          data: item,
          boolData: bool
        }
      });
  
      dialogRef.afterClosed();
    }
  
}
