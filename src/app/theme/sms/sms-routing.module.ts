import { NgModule } from "../../../../node_modules/@angular/core";
import {Routes,RouterModule} from "@angular/router";
import { SmsComponent } from "./sms/sms.component";
const smsRoutes:Routes = [
    {
        path: "",
        component: SmsComponent
    }
]
@NgModule({
    imports:[RouterModule.forChild(smsRoutes)],
    exports: [RouterModule]
})
export class SmsRoutingModule{

}
