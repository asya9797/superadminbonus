import { Injectable } from "../../../../node_modules/@angular/core";
import { ApiService } from "../../services/api.service";

@Injectable()
export class SmsService{
    constructor( private _apiService: ApiService){}

    getSmsCount(){
        return this._apiService.get("count/sms")
    }
    getAllSms(page,count){
        return this._apiService.get(`getAll/sms/${page}/${count}`)
    }
    deleteSms(id){
        return this._apiService.delete(`client/delete/${id}`)
    }
}