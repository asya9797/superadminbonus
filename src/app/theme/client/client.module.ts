import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ClientService } from "./client.service";
import { ClientRoutingModule } from "./client-routing.module";
import { ClientComponent } from "./client/client.component";
import { CookieService } from "ngx-cookie-service";
import { MaterialModule } from "../../shared/material/material.module";
import { FormsModule } from "@angular/forms";
import { SharedModule } from "../../shared/shared.module";
@NgModule({
    declarations:[ClientComponent],
    imports:[CommonModule,ClientRoutingModule,MaterialModule,FormsModule,SharedModule],
    exports:[],
    providers:[ClientService,CookieService]
})
export class ClientModule{}