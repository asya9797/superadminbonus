import { Component, OnInit } from '@angular/core';
import { ClientService } from '../client.service';
import { AppService } from '../../../services/app.service';
import { DatePipe } from '@angular/common';
import { MessageService } from 'primeng/components/common/messageservice';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss']
})
export class ClientComponent implements OnInit {
  public loading: boolean = false;
  public totalCount: number = 0;
  public count: number = 10;
  public page: number = 0;
  public _clientInfo: any;
  public rangeDates: Array<Date>;
  public sortBy: string = 'Summa';
  public descent: string = '+';
  public forFilterClient: boolean = false;
  public isClientsAvailable: boolean = false;
  constructor(private _clientService: ClientService, private _appService: AppService, private _datePipe: DatePipe, private _messageService: MessageService) { }


  ngOnInit() {
    this.getClientsData(this.sortBy, this.descent);
  }
  getClients(first, second) {
    this.sortBy = first;
    this.descent = second;
    return this._clientService.sortClientsBySale(first, second, this.page, this.count).map((data: any) => {
      this._clientInfo = data.data;
      console.log("client data is ------", this._clientInfo);
      return data;
    })
  }


  private _getClientCount() {
    return this._clientService.getClientCount().map((data: any) => {
      console.log("client count", data);
      this.totalCount = data.data;
      this.isClientsAvailable = true;
      console.log(this.totalCount)
      return data
    })
  }


  public getClientsData(first, second) {
    this.loading = true;
    const combined = Observable.forkJoin(
      this._getClientCount(),
      this.getClients(first, second)
    )
    combined.subscribe((data) => {
      console.log(data);
      this.loading = false;
    })
  }


  filterClients() {
    this.forFilterClient = true;
    if (this.rangeDates && this.rangeDates[0] && this.rangeDates[1]) {
      let startDate = this._datePipe.transform(this.rangeDates[0], 'yyyy-MM-dd');
      let endDate = this._datePipe.transform(this.rangeDates[1], 'yyyy-MM-dd');
      this._clientService.filterClientsByDate(this.sortBy, this.descent, startDate, endDate, this.page, this.count).subscribe((data: any) => {
        this._clientInfo = data.data;
        console.log(this._clientInfo)
      })
    }
  }


  private _getFilterClientCount() {
    if (this.rangeDates && this.rangeDates[0] && this.rangeDates[1]) {
      let startDate = this._datePipe.transform(this.rangeDates[0], 'yyyy-MM-dd');
      let endDate = this._datePipe.transform(this.rangeDates[1], 'yyyy-MM-dd');
      return this._clientService.filterClientsByDateCount(startDate, endDate).map((data: any) => {
        this.totalCount = data.data;
      })
    }
    else {
      return Observable.of([]);
    }
  }

  public paginate(event) {
    console.log("eventyy", event);
    this.count = event.pageSize;
    this.page = event.pageIndex;
    if (!this.forFilterClient) {
      return this._clientService.sortClientsBySale(this.sortBy, this.descent, this.page, this.count).map((data: any) => {
        this._clientInfo = data.data;
        console.log("client info now", this._clientInfo);
      })
    }
    if (this.forFilterClient) {
      if (this.rangeDates && this.rangeDates[0] && this.rangeDates[1]) {
        let startDate = this._datePipe.transform(this.rangeDates[0], 'yyyy-MM-dd');
        let endDate = this._datePipe.transform(this.rangeDates[1], 'yyyy-MM-dd');
        return this._clientService.filterClientsByDate(this.sortBy, this.descent, startDate, endDate, this.page, this.count).map((data: any) => {
          this._clientInfo = data.data;
          console.log(this._clientInfo)

        })
      }
    }
  }

  public getPaginateClientsData(event) {

    const combined = Observable.forkJoin(
      this._getFilterClientCount(),
      this.paginate(event)
    )
    this.loading = true;
    combined.subscribe((data) => {
      console.log(data);
      this.loading = false;
    })
  }


  onClickDelete(id): void {
    this._appService.confirmDialog().subscribe((data) => {
      if (data && data.approve) {
        this.deleteClientById(id)
      }
    })
  }

  deleteClientById(id) {
    this._clientService.deleteClient(id).subscribe((data: any) => {
      console.log("deleted");
      this.getClientsData(this.sortBy, this.descent);
      this._messageService.add({ severity: 'success', summary: '', detail: "This client deleted" })
    })
  }



  //clienti filtri inputi arjeqy maqrelu hamar e
  deleteClientDate() {
    this.rangeDates = undefined;
    this.forFilterClient = false;
    this.getClientsData(this.sortBy, this.descent);
  }
}
