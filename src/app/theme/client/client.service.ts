import { Injectable } from "@angular/core";
import { ApiService } from "../../services/api.service";

@Injectable()
export class ClientService{
    
    constructor(private _apiService: ApiService){}

    sortClientsBySale(first,second,page,count){
        return this._apiService.get(`sortClientsBySale/${first}/${second}/${page}/${count}`) 
    }
    deleteClient(id){
        return this._apiService.delete(`delete/client/${id}`)
    }
    filterClientsByDate(first,second,startDate,endDate,page,count){
        return this._apiService.get( `filterClientsByDate/${first}/${second}/${startDate}/${endDate}/${page}/${count}`)
    }
    getClientCount(){
        return this._apiService.get('client/count')
    }
    filterClientsByDateCount(startDate,endDate){
        return this._apiService.get(`count/filterClientsByDate/${startDate}/${endDate}`)
    }
}