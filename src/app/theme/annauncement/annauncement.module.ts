import { NgModule } from "@angular/core";
import { AnnauncementComponent } from "./annauncement/annauncement.component";
import { CommonModule } from "@angular/common";
import { MaterialModule } from "../../shared/material/material.module";
import { AnnauncementRoutingModule } from "./annauncement-routing.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AnnauncementService } from "./annauncement.service";
import { CookieService } from "ngx-cookie-service";
import { ViewTextComponent } from './view-text/view-text.component';
@NgModule({
    declarations:[AnnauncementComponent],
    imports:[CommonModule,AnnauncementRoutingModule,MaterialModule,FormsModule,ReactiveFormsModule],
    exports:[],
    providers:[AnnauncementService,CookieService]
})
export class AnnauncementModule{}