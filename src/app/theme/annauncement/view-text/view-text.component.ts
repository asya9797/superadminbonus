import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { FormGroup} from '@angular/forms';
@Component({
  selector: 'app-view-text',
  templateUrl: './view-text.component.html',
  styleUrls: ['./view-text.component.scss']
})
export class ViewTextComponent implements OnInit {
  public infoForm: FormGroup;
  public baseUrl = 'http://46.101.179.50:3000/static/'
  constructor( public dialogRef: MatDialogRef<ViewTextComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
  }

 
  onNoClick() {
    this.dialogRef.close();
  }
  getImage(item){
    let baseUrl = 'http://46.101.179.50:3000/static/'
    if(item){
      return {
        'background-image': `url(${baseUrl}${item})`
      }
    }else {
      return {
        'background-image': 'url("/assets/def.png")'
      }
    }
  }

}
