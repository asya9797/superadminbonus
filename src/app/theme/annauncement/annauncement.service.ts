import { Injectable } from "@angular/core";
import { ApiService } from "../../services/api.service";

@Injectable()
export class AnnauncementService{
    constructor(private _apiService: ApiService){}


    getAnnouncementCount(){
        return this._apiService.get(`get/announcements/count`)
    }
    getAllAnnouncement(page,count){
        return this._apiService.get(`getAll/announcements/${page}/${count}`)
    }
    deleteAnnouncement(id){
        return this._apiService.delete(`announcements/delete/${id}`)
    }
}