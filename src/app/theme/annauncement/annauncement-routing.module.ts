import { NgModule } from "@angular/core";
import {Routes,RouterModule} from "@angular/router";
import { AnnauncementComponent } from "./annauncement/annauncement.component";
const annauncementRoutes: Routes =[
    {
        path: "",
        component: AnnauncementComponent
    }
]
@NgModule({
    imports:[RouterModule.forChild(annauncementRoutes)],
    exports:[RouterModule]
})
export class AnnauncementRoutingModule{

}