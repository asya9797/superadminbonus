import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnnauncementComponent } from './annauncement.component';

describe('AnnauncementComponent', () => {
  let component: AnnauncementComponent;
  let fixture: ComponentFixture<AnnauncementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnnauncementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnnauncementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
