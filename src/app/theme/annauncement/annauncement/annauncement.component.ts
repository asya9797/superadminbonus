import { Component, OnInit, Inject } from '@angular/core';
import { AnnauncementService } from '../annauncement.service';
import { DatePipe } from '@angular/common';
import { AppService } from '../../../services/app.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { ViewTextComponent } from '../view-text/view-text.component';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
@Component({
  selector: 'app-annauncement',
  templateUrl: './annauncement.component.html',
  styleUrls: ['./annauncement.component.scss']
})
export class AnnauncementComponent implements OnInit {
  public totalCount: number = 0;
  public page:number = 0;
  public count:number = 8;
  public annauncement:boolean = true
  public annauncementInfo: any;
  // public image: string= "/assets/def.png"
  public loading: boolean = false
  // public baseUrl = 'http://46.101.179.50:3000/static/'
  constructor(private _annauncementService: AnnauncementService, private _datePipe: DatePipe, private _appService: AppService, private _messageService: MessageService,public dialog: MatDialog,) { }

  ngOnInit() {
    this.annauncementCount();
    this.getAnnauncement()
  }

  annauncementCount(){
    this._annauncementService.getAnnouncementCount().subscribe((data:any) => {
      console.log("totalcountyyyy",data)
      this.totalCount = data.data[0].count;
      console.log(this.totalCount)
    })
  }
  getAnnauncement(){
    this.loading = true;
    this._annauncementService.getAllAnnouncement(this.page,this.count).subscribe((data:any) => {
      for (let i of data.data) {
        let startTime = i.StartTime;
        let endTime = i.EndTime;
        let createTime = i.CreateTime;
        i["CreateTime"] = this._datePipe.transform(createTime, 'dd-MM-yyyy');
        i["StartTime"] = this._datePipe.transform(startTime, 'dd-MM-yyyy');
        i["EndTime"] = this._datePipe.transform(endTime, 'dd-MM-yyyy');
      }
        this.annauncementInfo = data.data;
        this.loading = false;
        console.log(this.annauncementInfo)
    })
  }
  public paginate(event){
    this.count = event.pageSize;
    this.page = event.pageIndex;
    this.loading = true;
     this._annauncementService.getAllAnnouncement( this.page, this.count).subscribe((data: any) => {
      for (let i of data.data) {
        let startTime = i.StartTime;
        let endTime = i.EndTime;
        let createTime = i.CreateTime;
        i["CreateTime"] = this._datePipe.transform(createTime, 'dd-MM-yyyy');
        i["StartTime"] = this._datePipe.transform(startTime, 'dd-MM-yyyy');
        i["EndTime"] = this._datePipe.transform(endTime, 'dd-MM-yyyy');
      }
        this.annauncementInfo = data.data;
        this.loading = false;
      })
    }
    

    onClickDelete(id): void {
      this._appService.confirmDialog().subscribe((data) => {
        if (data && data.approve) {
          this.deleteAnnouncementById(id)
        }
      })
    }
  
    deleteAnnouncementById(id) {
      this._annauncementService.deleteAnnouncement(id).subscribe((data: any) => {
        console.log("deleted");
        this.getAnnauncement();
        this.annauncementCount();
        this._messageService.add({ severity: 'success', summary: '', detail: "Этот объявление был удален" })
      })
    }
    openDialog(item,bool): void {
      const dialogRef = this.dialog.open(ViewTextComponent, {
        width: '500px',
        data: {
          data: item,
          boolData: bool    
        },
        panelClass: "forPadding"

      });
  
      dialogRef.afterClosed();
    }
    getImage(item){
      let baseUrl = 'http://46.101.179.50:3000/static/'
      if(item){
        return {
          'background-image': `url(${baseUrl}${item})`
        }
      }else {
        return {
          'background-image': 'url("/assets/def.png")'
        }
      }
    }
  

}
