import {Injectable} from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  target?: boolean;
  name: string;
  type?: string;
  children?: ChildrenItems[];
}

export interface MainMenuItems {
  state: string;
  short_label?: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

export interface Menu {
  label?: string;
  main: MainMenuItems[];
}

const MENUITEMS = [
  {
    main: [
      {
        state: 'parliament',
        short_label: 'P',
        name: 'Парламенты',
        type: 'link',
        icon: 'feather icon-user'
      },
      {
            state: 'client',
            short_label: 'C',
            name: 'Клиенты',
            type: 'link',
            icon: 'feather icon-users'
          },
          {
            state: 'provider',
            short_label: 'P',
            name: 'Провайдеры',
            type: 'link',
            icon: 'fa fa-connectdevelop'
          },
          {
            state: 'statistics',
            short_label: 'S',
            name: 'Статистика',
            type: 'link',
            icon: 'feather icon-bar-chart-2'
          },
          {
            state: 'map',
            short_label: 'M',
            name: 'Карта',
            type: 'link',
            icon: 'feather icon-map'
          },
          {
            state: 'admin',
            short_label: 'A',
            name: 'Админы',
            type: 'link',
            icon: 'feather icon-plus-square'
          },
          {
            state: 'annauncement',
            short_label: 'A',
            name: 'Объявление',
            type: 'link',
            icon: 'fa fa-bullhorn'
          },
          {
            state: 'sms',
            short_label: 'M',
            name: 'Сообщение',
            type: 'link',
            icon: 'feather icon-message-square'
          }
         
    ]
  }
  
];


@Injectable()
export class MenuItems {
  getAll(): Menu[] {
    return MENUITEMS;
  }
}
