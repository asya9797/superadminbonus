import { NgModule } from "@angular/core";
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDialogModule } from '@angular/material/dialog';
import { ChartModule } from 'primeng/chart';
import 'hammerjs'
import { MatButtonModule, MatPaginatorModule, MatInputModule } from "@angular/material";
import { CalendarModule } from "primeng/calendar";
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner'
import { GrowlModule } from 'primeng/growl';


@NgModule({
    imports: [MatExpansionModule, MatFormFieldModule, MatDialogModule, MatButtonModule, CalendarModule, ChartModule, MatSlideToggleModule, MatProgressSpinnerModule, GrowlModule, MatPaginatorModule, MatInputModule],
    exports: [MatExpansionModule, MatFormFieldModule, MatDialogModule, MatButtonModule, CalendarModule, ChartModule, MatSlideToggleModule, MatProgressSpinnerModule, GrowlModule, MatPaginatorModule, MatInputModule]
})
export class MaterialModule { }