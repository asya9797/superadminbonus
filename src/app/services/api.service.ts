import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { CookieService } from 'angular2-cookie/services/cookies.service';

@Injectable()
export class ApiService {
    // private _baseUrl: string = "http://46.101.179.50:3000/admin/";
    private _baseUrl: string = "http://46.101.179.50:3000/admin/";
    
    constructor(private _httpClient:HttpClient,private _cookieService:CookieService){}


    public get(url:string){
        let adminToken = this._cookieService.get("adminToken");
        let headers = new HttpHeaders({
            'Content-type': 'application/json',
            'x-access-token': adminToken
        })
        return this._httpClient.get(this._baseUrl+url,{headers:headers});
    }

    public getJS(url:string){
        let adminToken = this._cookieService.get("adminToken");
        let headers = new HttpHeaders({
            'Content-type': 'application/json',
            'x-access-token': adminToken
        })
        return this._httpClient.get(this._baseUrl+url,{headers:headers,observe:'response'});
    }

    public post(url:string,body:object){ 
        let adminToken = this._cookieService.get("adminToken");
        let headers = new HttpHeaders({
            'Content-type': 'application/json',
            'x-access-token': adminToken
        })
        return this._httpClient.post(this._baseUrl+url,body,{headers:headers})
    }
    public postImage(url:string,body){
        let adminToken = this._cookieService.get("adminToken");
        let headers = new HttpHeaders({
            'x-access-token': adminToken
        })
        return this._httpClient.post(this._baseUrl+url,body,{headers:headers})
    }

    public postJS(url:string,body:object){
        let adminToken = this._cookieService.get("adminToken");
        let headers = new HttpHeaders({
            'Content-type': 'application/json',
            'x-access-token': adminToken
        })
        return this._httpClient.post(this._baseUrl+url,body,{headers:headers,observe:'response'})
    }

    public put(url:string,body:object){
        let adminToken = this._cookieService.get("adminToken");
        let headers = new HttpHeaders({
            'Content-type': 'application/json',
            'x-access-token': adminToken
        })
        return this._httpClient.put(this._baseUrl+url,body,{headers:headers})
    }

    public putJS(url:string,body:object){
        let adminToken = this._cookieService.get("adminToken");
        let headers = new HttpHeaders({
            'Content-type': 'application/json',
            'x-access-token': adminToken
        })
        return this._httpClient.put(this._baseUrl+url,body,{headers:headers,observe:'response'})
    }

    public delete(url:string){
        let adminToken = this._cookieService.get("adminToken");
        let headers = new HttpHeaders({
            'Content-type': 'application/json',
            'x-access-token': adminToken
        })
        return this._httpClient.delete(this._baseUrl+url,{headers:headers})
    }

    public deleteJS(url:string){
        let adminToken = this._cookieService.get("adminToken");
        let headers = new HttpHeaders({
            'Content-type': 'application/json',
            'x-access-token': adminToken
        })
        return this._httpClient.delete(this._baseUrl+url,{headers:headers,observe:'response'})
    }
    public checkPassword(userinfo){
        let adminToken = this._cookieService.get("adminToken");
        let headers = new HttpHeaders({
            'Content-type': 'application/json',
            'x-access-token': adminToken
        })
        return this._httpClient.post( this._baseUrl + "check", userinfo,{headers: headers});
    }
}