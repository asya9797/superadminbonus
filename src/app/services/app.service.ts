import { Injectable } from '@angular/core';
import { ConfirmComponent } from '../theme/admin/confirm/confirm.component';
import { MatDialog } from '@angular/material';
import 'rxjs/add/operator/map';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AppService {
    // private baseUrl: string = "http://annaniks.com:3000/admin/"
    constructor(private dialog: MatDialog) { }


    public confirmDialog() {
        const dialogRef = this.dialog.open(ConfirmComponent, {
            width: '460px',
            panelClass: "forPadding",

        })
        return dialogRef.afterClosed().map(result => {
            return result;
        })
    }
    
}