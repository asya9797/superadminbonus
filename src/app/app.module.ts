import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { AdminComponent } from './layout/admin/admin.component';
import { AuthComponent } from './layout/auth/auth.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from './shared/shared.module';
import { MenuItems } from './shared/menu-items/menu-items';
import { BreadcrumbsComponent } from './layout/admin/breadcrumbs/breadcrumbs.component';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { AuthGuard } from './theme/authGuard/authGuard.service';
import { ApiService } from './services/api.service';
import { AppService } from './services/app.service';
import { MatButtonModule } from '@angular/material';
import { MaterialModule } from './shared/material/material.module';
import { ConfirmComponent } from './theme/admin/confirm/confirm.component';
import { AdminService } from './theme/admin/admin.service';
import { DatePipe } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MessageService } from 'primeng/components/common/messageservice';
import { ViewTextComponent } from './theme/annauncement/view-text/view-text.component';

@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    AuthComponent,
    BreadcrumbsComponent,
    ConfirmComponent,
    ViewTextComponent,
    
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    SharedModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [MenuItems, CookieService, AuthGuard, ApiService, AppService, AdminService, DatePipe, MessageService],
  bootstrap: [AppComponent],
  entryComponents: [ConfirmComponent, ViewTextComponent]
})
export class AppModule { }
